<?php
namespace ticmakers\adm\migrations;
 /**
 * Migración m190225_230103_SocialNetworks implementa las acciones para la creación de la tabla social_networks.
 *
 * @package ticmakers\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */

class m190225_230103_SocialNetworks extends \yii\db\Migration
{
    public $tableName = 'social_networks';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'social_network_id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'social_network_user' => $this->string(128)->notNull(),
            'name' => $this->char(3)->notNull(),
            'response' => $this->text(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[user_id]]) REFERENCES users ([[user_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'social_network_id', "Social network identifier");

        $this->addCommentOnColumn($this->tableName, 'user_id', "User identifier");

        $this->addCommentOnColumn($this->tableName, 'social_network_user', "User identifier at social network ");

        $this->addCommentOnColumn($this->tableName, 'name', "Abbreviation of the name of the social network FAB -> Facebook; GOP -> Google Plus.");

        $this->addCommentOnColumn($this->tableName, 'response', "Information provided by the social network");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y -> Yes, N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
