<?php
namespace ticmakers\adm\migrations;
 /**
 * Migración m190225_230103_AuthItemChild implementa las acciones para la creación de la tabla auth_item_child.
 *
 * @package ticmakers\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */

class m190225_230103_AuthItemChild extends \yii\db\Migration
{
    public $tableName = 'auth_item_child';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'auth_item_child_id' => $this->primaryKey(),
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[child]]) REFERENCES auth_item ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'auth_item_child_id', "Auth item child identifier");

        $this->addCommentOnColumn($this->tableName, 'parent', "Item parent identifier");

        $this->addCommentOnColumn($this->tableName, 'child', "Item child identifier");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y -> Yes, N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', " It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
