<?php
namespace ticmakers\adm\migrations;

/**
 * Migración m190226_230105_InsertDefaultUsers
 *
 * @package ticmakers\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */

class m190226_230105_InsertDefaultUsers extends \yii\db\Migration
{
    public $tableName = 'users';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        /**
         * Usuario: invitado
         * Contraseña: invitado
         */
        $this->insert($this->tableName, [
            'user_id' => 0,
            'username' => 'invitado',
            'email' => 'invitado@sistema.local',
            'password_hash' => '$2y$13$wYUPiMLf9esMEM3teYXdmuec2yg6X3EGwJdd/22.14XjgFU5E6bvi',
            'auth_key' => '9CzTL3n3lpXAzrEzih2ok0CJHB5p5lRk',
            'password_reset_token' => '',
            'email_key' => '',
            'confirmed_email' => 'Y',
            'blocked' => 'N',
            'accepted_terms' => 'Y',
            'active' => 'Y',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        /**
         * Usuario: sistema
         * Contraseña: sistema2019
         */
        $this->insert($this->tableName, [
            'user_id' => 1,
            'username' => 'sistema',
            'email' => 'sistema@sistema.local',
            'password_hash' => '$2y$13$BO.F27gpwJa.dWTMMAJDS.2HqKHADSmRHcwz7icryejD4eOMAhPB6',
            'auth_key' => 'Vsc_H1XZCJAE288xnuZ229bTnBKgXQPj',
            'password_reset_token' => '',
            'email_key' => '',
            'confirmed_email' => 'Y',
            'blocked' => 'N',
            'accepted_terms' => 'Y',
            'active' => 'Y',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    public function down()
    {
        $this->delete($this->tableName, 'user_id in (0,1)');
    }
}
