<?php
use ticmakers\core\base\Modal;use ticmakers\core\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $form ticmakers\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

$this->title = Yii::t($moduleId, 'Session expired');

?>

<div class="row justify-content-md-center align-items-center align-self-center m-0" style="height: 100%">

    <div class="col col-md-8 align-self-center align-middle">        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
                <div class="content-fluid"></div>
                <div class="card my-5 px-5 pb-5">
                    <div class="card-body">

                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <span class="avatar avatar-online">
                                        <?=Yii::$app->html::img("{$urlAssets}/imgs/user.jpg", ['style' => 'width:10rem;'])?>

                                    </span>
                                </div>
                                <div class="col-9">
                                    <h3><?=Yii::t($moduleId, 'YOUR SESSION HAS EXPIRED');?></h3>
                                    <p><?=Yii::t($moduleId, 'Due to inactivity, your session has been closed');?></p>
                                    <p><?=Yii::t($moduleId, 'Enter your password to continue');?></p>
                                </div>
                            </div>
                        </div>

                        <div class="security-expired-session">

                            <?php $form = ActiveForm::begin([
    'id' => 'expired-session-form',
]);?>
                            <div class="row pt-5">
                                <div class="col-12 text-left">
                                    <?=$form->field($model, 'email', ['help' => '', 'popover' => $model->getHelp('email')])
->textInput([
    'autofocus' => true,
    'disabled' => true,
])?>
                                </div>
                                <div class="col-12 text-left">
                                    <?=$form->field($model, 'password', ['help' => '', 'popover' => $model->getHelp('password')])->passwordInput()?>
                                </div>

                            </div>

                            <div class="col-lg-12 mt-2">

                                <div class="container-button col-8 offset-md-2">
                                    <?=Yii::$app->html::submitButton(Yii::t($moduleId, 'Log in'), ['class' => 'btn btn-primary btn-block btn-md mt-40', 'name' => 'register-button'])?>
                                </div>

                            </div>
                            <div class="col-lg-12 mt-2">
                                <a class="float-center" style="margin-left: 0px" href="<?php echo Url::to(Yii::$app->user->loginUrl) ?>"><?=Yii::t($moduleId, 'you are not {username}?', ['username'=>$model->username])?></a>
                            </div>
                            <?php ActiveForm::end();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
