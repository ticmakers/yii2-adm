<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\registerForm */
use ticmakers\core\widgets\ActiveForm;
use ticmakers\core\base\Modal;
use yii\helpers\Url;

$this->title = Yii::t($moduleId, 'Register');
?>

<div class="row justify-content-md-center align-items-center align-self-center m-0" style="height: 100%">

    <div class="col col-md-8 align-self-center align-middle">        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="vertical-align-middle">
                <div class="card py-20 px-25">
                    <div class="card-body">

                        <h2 class="card-title mb-5"><?= Yii::t($moduleId, 'Create an account') ?></h2>
                        <div class="site-register">

                            <?php $form = ActiveForm::begin([
                                'id' => 'register-form',
                                'enableClientValidation' => true
                            ]); ?>
                            <div class="row pt-15">
                                <div class="col-sm-6 col-md-6 col-lg-6 text-left">
                                    <?= $form->field($model, 'username', ['help' => '', 'popover' => $model->getHelp('username')])->textInput(['autofocus' => true]) ?>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6 text-left">
                                    <?= $form->field($model, 'email', ['help' => '', 'popover' => $model->getHelp('email')])->textInput([]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6 text-left">
                                    <?= $form->field($model, 'password', ['help' => '', 'popover' => $model->getHelp('password')])->passwordInput() ?>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6 text-left">
                                    <?= $form->field($model, 'confirmPassword', ['help' => '', 'popover' => $model->getHelp('confirmPassword')])->passwordInput() ?>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-2 p-0 text-left">
                                <?php 
                                $termsConditionsLink = Yii::$app->html::a(Yii::t($moduleId, 'terms and conditions'), ['security/terms-conditions'], 
                                [
                                    'onclick' => '$("#terms-conditions-modal").modal({show: "false"}); return false;', 
                                    'class' => 'terms-conditions-link', 
                                    'data-target' => '#terms-conditions-modal', 
                                    'style' => 'margin-left: 0px'
                                ])?>
                                <?= $form->field($model, 'accepted_terms')->checkbox([
                                    'template' => "{input} {label} {error}",
                                    ])->label(Yii::t($moduleId, "I have read and accept the").' '. $termsConditionsLink)?>

                            </div>
                            <div class="col-8 offset-2 d-flex justify-content-center">
                                <?= Yii::$app->html::submitButton(Yii::t($moduleId, 'Register account'), ['class' => 'btn btn-primary btn-block btn-md mt-20', 'name' => 'register-button']) ?>
                            </div>
                            <div class="col-lg-12 mt-2">
                                <p><?= Yii::t($moduleId, 'Do you already have an account?') ?></p>
                                <a class="float-center" style="margin-left: 0px" href="<?php echo Url::to(['security/login']) ?>"><?= Yii::t($moduleId, 'Return to login') ?></a>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php Modal::begin([
    'id' => 'terms-conditions-modal',
    'header' => Yii::t($moduleId, 'Terms and conditions'),
    'size' => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t($moduleId, 'Ok'), Yii::$app->html::ICON_REMOVE),
    'options' => ['style' => 'display: none;', 'tabindex' => false],
]);
echo $this->render('terms_conditions');
Modal::end();
?> 