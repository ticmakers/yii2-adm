<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\registerForm */

use ticmakers\core\widgets\ActiveForm;

use yii\web\View;
use yii\helpers\Url;

$this->title = Yii::t($model->module->id, 'Login');
?>
<script src="<?= Yii::$app->controller->module->socialAuthConfig['firebaseUrl'] ?>"></script>
<div class="row justify-content-center align-items-center align-self-center m-0" style="height: 100%">
    <div class="col col-md-4 align-self-center align-middle">
        <div class="vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="vertical-align-middle">
                <div class="content-fluid"></div>
                <div class="card px-20 py-25">
                    <div class="card-body pt-15">
                        <h2 class="card-title mb-5"><?= Yii::t($model->module->id, 'Ingresar al sistema') ?></h2>
                        <div class="site-login mt-10">
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                            ]); ?>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-left">
                                    <?= $form->field($model, $model::USERNAME, ['help' => '', 'popover' => $model->getHelp($model::USERNAME)])->textInput(['autofocus' => false]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-left">
                                    <?= $form->field($model, $model::PASS, ['help' => '', 'popover' => $model->getHelp($model::PASS)])->passwordInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 text-left">
                                    <?= $form->field($model, $model::REMEMBERME, [ 'help' => '', 'popover' => $model->getHelp($model::REMEMBERME) ])->checkbox(); ?>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <a href="<?php echo Url::to(['security/restore-password']) ?>"><?= Yii::t($model->module->id, 'Forgot your password?') ?></a>
                                </div>
                                <div class="container-button col-12">
                                    <?= Yii::$app->html::submitButton(Yii::t($model->module->id, 'Log in'), ['class' => 'btn btn-primary btn-block btn-md mt-10', 'name' => 'login-button']) ?>
                                </div>
                                <h5 class="login-letter-separator">ó</h5>
                                <div class="container-button col-12">
                                    <a href="#" class="btn btn-google btn-block btn-md" data-socialtype="GOP" onclick="setLogin(this, event)"><?= Yii::t($model->module->id, 'Log in with Google') ?></a>
                                </div>
                                <div class="container-button col-12">
                                    <a href="#" class="btn btn-facebook btn-block btn-md mt-2" data-socialtype="FAB" onclick="setLogin(this, event)"><?= Yii::t($model->module->id, 'Log in with Facebook') ?></a>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$urlSocialLogin = Url::to('login');
$variables = Yii::$app->controller->module->socialAuthConfig;
unset($variables['firebaseUrl']);
$variables = json_encode($variables);
$this->registerJs(<<<JS
    var config = $variables;
    firebase.initializeApp(config);
    
    function setLogin(btn, event) {
        btn = $(btn);
        let provider = getProvider(btn.attr('data-socialtype'));
        btn.attr('disabled', true);
        firebase.auth().signInWithPopup(provider).then((result) => {
            sendLogin(result, btn.attr('data-socialtype'));
            btn.attr('disabled', false);
        }).catch((error) => {
            console.log(error);
            btn.attr('disabled', false);
        });
        event.preventDefault();
    }

    function getProvider(type) {
        let provider = null;
        switch (type) {
            case 'FAB':
                break;
            case 'GOP': default:
                provider = new firebase.auth.GoogleAuthProvider();
                provider.addScope('email');
                break;
        }
        return provider;
    }

    function sendLogin(data, type) {
        let method = 'post';
        let form = document.createElement('form');
        let attributes = [
            { name: 'username', value: data.additionalUserInfo.profile.name },
            { name: 'email', value: data.additionalUserInfo.profile.email },
            { name: 'name', value: type },
            { name: 'response', value: JSON.stringify(data) },
            { name: 'social_network_user', value: data.additionalUserInfo.profile.id },
            { name: '_csrf', value: $('input[name="_csrf"]').val() }
        ];
        form.setAttribute('method', method);
        form.setAttribute('style', 'display: none;');
        form.setAttribute('action', '$urlSocialLogin');
        for (let attribute of attributes) {
            let hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', attribute.name);
            hiddenField.setAttribute('value', attribute.value);
            form.appendChild(hiddenField);
        }
        document.body.appendChild(form);
        form.submit();
    }
JS
, View::POS_END); ?>