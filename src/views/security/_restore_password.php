<?php

use ticmakers\core\widgets\ActiveForm;
use yii\helpers\Url;/* @var $this yii\web\View */
/* @var $model User */
/* @var $form ticmakers\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

$this->title = Yii::t($model->module->id, 'Restore password');

?>

<div class="row justify-content-center align-items-center align-self-center m-0" style="height:100%">

    <div class="col col-md-6 align-self-center align-middle">

        <div class="vertical-align text-center">
            <div class="vertical-align-middle">
                <div class="card py-10 px-20">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><?= Yii::t($model->module->id, 'Recover your password by entering the following data:') ?></p>
                            </div>
                        </div>
                        <div class="security-restore-password">

                            <?php $form = ActiveForm::begin([
                                'id' => 'restore-password-form',
                            ]);?>
                            <div class="row pt-15">
                                <div class="col-12 text-left">
                                    <?=$form->field($model, 'email')
                                    ->textInput([
                                        'autofocus' => true
                                    ])?>
                                </div>
                            </div>

                            <div class="row">

                                <div class="container-button col-12">
                                    <?=Yii::$app->html::submitButton(Yii::t($model->module->id, 'Restore password'), ['class' => 'btn btn-primary btn-block btn-md', 'name' => 'register-button'])?>
                                </div>

                            </div>

                            <div class="col-lg-12 mt-2">
                                <a class="float-center" style="margin-left: 0px" href="<?php echo Url::to(['security/login']) ?>"><?=Yii::t($model->module->id, 'Return to login')?></a>
                            </div>
                            <?php ActiveForm::end();?>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>

</div>