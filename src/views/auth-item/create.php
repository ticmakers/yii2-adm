<?php
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */
$breadcrumbs = [];
$this->title = Yii::t($model->module->id, 'Create roles');
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="auth-item-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
