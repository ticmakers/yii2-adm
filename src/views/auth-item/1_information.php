<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\YiiAsset;
use ticmakers\adm\models\base\AuthItem;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */
/* @var $form ticmakers\widgets\ActiveForm */

YiiAsset::register($this);
if (!$model->isNewRecord) {
	$items = Json::htmlEncode([
		'roleItems' => $modelItems
	]);
	$this->registerJs("var _items = {$items}; var _parent = '$model->name'");
	$this->registerJs($this->render('_script.js'));
	$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
}
?>
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput(['maxlength' => true, 'tabindex' => 1]) ?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'description', ['help' => '', 'popover' => $model->getHelp('description')])->textarea(['rows' => 4, 'tabindex' => 2]) ?>
	</div>

</div>

<?php if (!$model->isNewRecord) : ?>
	<div class="col-12 assignment-index">
		<fieldset style="display: flex;">
			<legend><?= Yii::t($model->module->id, 'Assignment') ?></legend>

			<div class="row w-100" style="justify-content: center">
				<div class="col-sm-5">
					<input class="form-control search" data-target="available" placeholder="<?= Yii::t($model->module->id, 'Search for available'); ?>">
					<select id="available-list" multiple size="20" class="form-control list" data-target="available">
					</select>
				</div>

				<div class="col-sm-1">
					<br><br>
					<?= Yii::$app->html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => $model->name], [
						'class' => 'btn btn-success btn-assign',
						'data-target' => 'available',
						'title' => Yii::t($model->module->id, 'Assign'),
					]); ?><br><br>
					<?= Yii::$app->html::a('&lt;&lt;' . $animateIcon, ['revoke', 'id' => $model->name], [
						'class' => 'btn btn-danger btn-assign',
						'data-target' => 'assigned',
						'title' => Yii::t($model->module->id, 'Remove'),
					]); ?>
				</div>

				<div class="col-sm-5">
					<input class="form-control search" data-target="assigned" placeholder="<?= Yii::t($model->module->id, 'Search for assigned'); ?>">
					<select id="assigned-list" multiple size="20" class="form-control list" data-target="assigned">
					</select>
				</div>
			</div>
		</fieldset>
	</div>
<?php endif; ?>
<?php
$this->registerCss(
	'fieldset { 
			border:1px solid rgb(228, 234, 236); 
			border-radius: 3px; 
			padding: 10px  0 30px;
		}
		legend {  
			font-size:100%;
			border:none;		
			width: auto;
			border: 4px solid #fff;
		} '
);

?>