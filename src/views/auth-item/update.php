<?php
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($model->module->id, 'Update') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t($model->module->id, 'Update');
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id,  $model->name)];
?>
<div class="auth-item-update">
    <?php echo $this->render('_form', [
        'model' => $model,
        'modelItems' => $modelItems
    ]) ?>
</div>
