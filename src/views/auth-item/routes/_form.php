<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\YiiAsset;
use ticmakers\adm\models\base\AuthItem;
use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */
/* @var $form ticmakers\widgets\ActiveForm */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($model->module->id, 'Create routes');
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Routes')];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Create')];

YiiAsset::register($this);
$items = Json::htmlEncode([
    'routes' => $modelRoutes       
]);$this->registerJs("var _items = {$items};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
?>
<div class="row">
	<div class="col-sm-11">
		<div class="input-group">
			<input id="inp-route" type="text" class="form-control" 
				placeholder="<?=Yii::t($model->module->id, 'New route(s)');?>">
			<span class="input-group-btn">
				<?=Yii::$app->html::a(Yii::t($model->module->id, 'Add') . $animateIcon, ['create-route'], [
					'class' => 'btn btn-success',
					'id' => 'btn-new',
				]);?>
			</span>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-12 assignment-index">
	
		<div class="row w-100" style="justify-content: center">
			<div class="col-sm-5">
				<div class="input-group">
					<input class="form-control search" data-target="available"
					placeholder="<?=Yii::t($model->module->id, 'Search for available');?>">
					<span class="input-group-btn">
						<?=Yii::$app->html::a('<span class="fa fa-sync-alt"></span>', false, [
							'class' => 'btn btn-default',
							'data-target' => 'refresh',
							'id' => 'btn-refresh',
							'style' => 'left: -32px; z-index: 10;'
						]);?>
					</span>
				</div>
				<select id="available-list" multiple size="20" class="form-control list" data-target="available">
				</select>
			</div>

			<div class="col-sm-2" style="padding-left: 40px;">
				<br><br>
				<?=Yii::$app->html::a('&gt;&gt;' . $animateIcon, ['auth-item/assign-routes', 'id' => $model->name], [
					'class' => 'btn btn-success btn-assign',
					'data-target' => 'available',
					'title' => Yii::t($model->module->id, 'Assign'),
				]);?><br><br>
				<?=Yii::$app->html::a('&lt;&lt;' . $animateIcon, ['auth-item/revoke-routes', 'id' => $model->name], [
					'class' => 'btn btn-danger btn-assign',
					'data-target' => 'assigned',
					'title' => Yii::t($model->module->id, 'Remove'),
				]);?>
			</div>

			<div class="col-sm-5">
				<input class="form-control search" data-target="assigned"
					placeholder="<?=Yii::t($model->module->id, 'Search for assigned');?>">
				<select id="assigned-list" multiple size="20" class="form-control list" data-target="assigned">
				</select>
			</div>
		</div>
	</div>
	
</div>
<br>