<?php

use ticmakers\core\widgets\ActiveForm;
use ticmakers\adm\models\base\Users;
use ticmakers\adm\helpers\Strings;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\searchs\AuthItem */
/* @var $form ticmakers\widgets\ActiveForm */
?>

<div class="auth-item-search">
    <?php $form = ActiveForm::begin([
        'id'     => 'auth-item-search-form',
        'action' => ['permission'],
        'method' => 'get',
    ]); ?>

    <div class="row">
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'name',['help' => '','popover' => $model->getHelp('name')])->textInput(['tabindex' => 1, 'id' => 'name_search']) ?>

			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'description',['help' => '','popover' => $model->getHelp('description')])->textInput(['tabindex' => 3, 'id' => 'description_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'rule_name',['help' => '','popover' => $model->getHelp('rule_name')])->textInput(['tabindex' => 4, 'id' => 'rule_name_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'data',['help' => '','popover' => $model->getHelp('data')])->textInput(['tabindex' => 5, 'id' => 'data_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'active',['help' => '','popover' => $model->getHelp('active')])->widget(\kartik\widgets\Select2::classname(),
                        [
                            'data' => Yii::$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 6, 'id' => 'active_search'],
                        ]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_by',['help' => '','popover' => $model->getHelp('created_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 7, 'id' => 'created_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 8, 'id' => 'created_at_search']]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 9, 'id' => 'updated_by_search']]
                    ) ?>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 8, 'id' => 'updated_at_search']]) ?>

			</div>
    </div>    <?php ActiveForm::end(); ?>
</div>
