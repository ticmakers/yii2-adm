
function showItems(list){
    $('i.glyphicon-refresh-animate').hide();
    $('#assigned-list, #available-list').html('')
    if(list.routes.available.length > 0){ 
        $('#available-list').append('<optgroup label="Routes"></optgroup>')  
        $.each( list.routes.available, function( id, item ) {
            $("#available-list > optgroup").append('<option value="' + item.name + '">' + item.name + '</option>');
        }); 
    }
    if(list.routes.assigned.length > 0){     
        $('#assigned-list').append('<optgroup label="Routes"></optgroup>') 
        $.each( list.routes.assigned, function( id, item ) {
            $("#assigned-list > optgroup").append('<option value="' + item.name + '">' + item.name + '</option>');
        });
    }
}

function updateItems(items) { 
    _items.routes.available = items.routes.available;
    _items.routes.assigned = items.routes.assigned;
    search('available');
    search('assigned');
}

$('.btn-assign').click(function () {
    var $this = $(this);
    var target = $this.data('target');
    var items = $('select.list[data-target="' + target + '"]').val();
    if (items && items.length) {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post($this.attr('href'), {items: items, parent: _parent}, function (items) {
            updateItems(items);
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});

$('.search[data-target]').keyup(function () {
    search($(this).data('target'));
}); 

function search(target) {
    var $list = $('select.list[data-target="' + target + '"]');
    $list.html('');
    var q = $('.search[data-target="' + target + '"]').val().toLowerCase();
    var groups = {
        role: [$('<optgroup label="Routes">'), false],
    };
    $.each(_items.routes[target], function (itemName, group) {
        let currentName = group.name.toLowerCase();
        if (currentName.indexOf(q) >= 0) {
            $('<option>').text(group.name).val(group.name).appendTo(groups.role[0]);
            groups.role[1] = true;
        }
    });
    $.each(groups, function () {
        if (this[1]) {
            $list.append(this[0]);
        }
    });

} 
showItems(_items)
