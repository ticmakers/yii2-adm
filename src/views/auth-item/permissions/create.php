<?php
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($model->module->id, 'Create permissions');

$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Permissions'), 'url' => ['auth-item/permission']];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Create')];
?>
<div class="auth-item-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
