<?php
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($model->module->id, 'Update') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Permissions'), 'url' => ['auth-item/permission']];
$this->params['breadcrumbs'][] = Yii::t($model->module->id, 'Update');
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
?>
<div class="auth-item-update">
    <?php echo $this->render('_form', [
        'model' => $model,
        'modelRoutes' => $modelRoutes
    ]) ?>
</div>
