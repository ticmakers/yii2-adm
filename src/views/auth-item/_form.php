<?php

use ticmakers\core\widgets\Tabs;
use ticmakers\core\widgets\ActiveForm;/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */
/* @var $form ticmakers\widgets\ActiveForm */
$form = ActiveForm::begin([
    'id' => 'auth-item-form',
]);
?>
<div class="auth-item-form">
    <?= Tabs::widget([
        'items' => [
            [
                'label'   => Yii::t($model->module->id, 'Información'),
                'content' => $this->render('1_information', ['form' => $form, 'model' => $model, 'modelItems' => !$model->isNewRecord ? $modelItems: []]),
                'active'  => true
            ],
        ],
    ]);
    ?>
    <br>
    <div class="form-group text-right">
        <?=  Yii::$app->ui::btnCancel(); ?>
        <?=  Yii::$app->ui::btnSend(null, ['form' => 'auth-item-form']); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
