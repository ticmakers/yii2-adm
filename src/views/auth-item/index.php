<?php
use kartik\dynagrid\DynaGrid;
use ticmakers\core\base\Modal;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\adm\models\searchs\AuthItem */
/* @var $dataProvider yii\data\ActiveDataProvider */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($searchModel->module->id, 'Roles');
$this->params['breadcrumbs'][] = ['label' => Yii::t($searchModel->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">
        <?php echo DynaGrid::widget([
            'columns' => [
                ['class'=>'kartik\grid\SerialColumn'],
                				[
					'attribute' => 'name',
					'visible' => true,
				],
				/* [
					'attribute' => 'type',
					'visible' => true,
				], */
				[
					'attribute' => 'description',
					'format' => 'ntext',
					'visible' => true,
				],
				[
					'attribute' => 'rule_name',
					'visible' => false,
				],
				[
					'attribute' => 'data',
					'format' => 'ntext',
					'visible' => false,
				],
				[
					'attribute' => 'active',
					'visible' => false,
					'value' => function($model){
						return Yii::$app->strings::getCondition($model->active);
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => Yii::$app->strings::getCondition(),
					'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'created_by',
					'visible' => false,
					'value' => function($model){
						return $model->creadoPor->username;
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => []/* Yii::$app->userHelper::getUsers() */,
					'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'created_at',
					'visible' => false,
					'filterType' => GridView::FILTER_DATE,
					'filterInputOptions' => ['id' => 'created_at_grid'],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'updated_by',
					'visible' => false,
					'value' => function($model){
						return $model->modificadoPor->username;
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => []/* Yii::$app->userHelper::getUsers() */,
					'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'updated_at',
					'visible' => false,
				],
                [
                    'class' => 'ticmakers\core\base\ActionColumn',
                    
                ],
            ],
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'toolbar' => [
                    '{toggleData}',
                    [
                        'content' =>
                        	Yii::$app->ui::btnNew('') . ' '
                            .Yii::$app->ui::btnSearch('') . ' '
                            .Yii::$app->ui::btnRefresh('')
                    ],
                ],
                'pjax' => true,
				'options' => ['id' => 'gridview-auth-item'],
				'panelHeadingTemplate' => '<h3>' . (Yii::t($searchModel->module->id, $this->title)) . '</h3>'
            ],
            'options' => ['id' => 'dynagrid-auth-item'],
        ]) ?>
        </div>
<?php Modal::begin([
    'id'      => 'search-modal',
    'header'  => ''.Yii::t($searchModel->module->id, 'Búsqueda Avanzada'),
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t($searchModel->module->id, 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t($searchModel->module->id, 'Buscar'),  ['form' => 'auth-item-search-form']),
    'options' => [ 'style' => 'display: none;', 'tabindex' => false ]
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
?>
