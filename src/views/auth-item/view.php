<?php

use ticmakers\helpers\Strings;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthItem */

 $this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Auth Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-view">

    <p>
        <?=  Yii::$app->ui::btnUpdate(null, ['update', 'id' => $model->name]); ?>
        <?=  Yii::$app->ui::btnDelete(null, ['delete', 'id' => $model->name]); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'type',
            'description:ntext',
            'rule_name',
            'data:ntext',
            [
                'attribute' => 'active',
                'value'     => Yii::$app->strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
