<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\MenuItems */

$this->title = Yii::t($model->module->id, 'Editar') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Menu Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->menu_item_id]];
$this->params['breadcrumbs'][] = Yii::t($model->module->id, 'Editar');
?>
<div class="menu-items-update">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
