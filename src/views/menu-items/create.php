<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\MenuItems */

$this->title = Yii::t($model->module->id, 'Crear Menu Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Menu Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-items-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
