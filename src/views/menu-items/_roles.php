<?php

use ticmakers\adm\helpers\Strings;
use ticmakers\adm\models\base\AuthItem;
use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\RoleMenuItems */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="menu-items">
    <?php $form = ActiveForm::begin([
    'id' => 'menu-items-form',
]);?>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?=$form->field($model, 'roles', [
    'help' => '',
    // 'popover' => $model->getHelp('roles')
])->widget(
    \kartik\widgets\Select2::classname(),
    [
        'data' => AuthItem::getData(true, ['type' => AuthItem::TYPE_ROL]),
        'options' => [
            'placeholder' => Yii::$app->strings::getTextEmpty(),
            'tabindex' => 1,
            'multiple' => true,
        ],
    ]
)?>

        </div>

    </div>    <?php ActiveForm::end();?>
</div>