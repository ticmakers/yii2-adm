<?php
use kartik\dynagrid\DynaGrid;
use ticmakers\core\base\Modal;
use kartik\grid\GridView;
use ticmakers\adm\models\base\Users;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\adm\models\searchs\MenuItems */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t($searchModel->module->id, 'Menu Items');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-items-index">
        <?php echo DynaGrid::widget([
            'columns' => [
                ['class'=>'kartik\grid\SerialColumn'],
                				[
					'attribute' => 'menu_item_id',
					'visible' => false,
				],
				[
					'attribute' => 'menu_id',
					'visible' => true,
					'value' => function($model) {
						if (!empty($model->menu)) {
							$name = $model->menu->getNameFromRelations();
							return $model->menu->$name;
						}
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => ticmakers\adm\models\base\Menus::getData(),
					'filterInputOptions' => ['id' => 'menu_id_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				],
				[
					'attribute' => 'name',
					'visible' => true,
				],
				[
					'attribute' => 'parent_menu_id',
					'visible' => true,
					'value' => function($model) {
						if (!empty($model->parentMenu)) {
							$name = $model->parentMenu->getNameFromRelations();
							return $model->parentMenu->$name;
						}
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => ticmakers\adm\models\base\MenuItems::getData(),
					'filterInputOptions' => ['id' => 'parent_menu_id_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				],
				[
					'attribute' => 'internal',
					'visible' => true,
				],
				[
					'attribute' => 'route_id',
					'visible' => true,
					'value' => function($model) {
						if (!empty($model->route)) {
							$name = $model->route->getNameFromRelations();
							return $model->route->$name;
						}
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => ticmakers\adm\models\base\AuthItem::getData(),
					'filterInputOptions' => ['id' => 'route_id_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				],
				[
					'attribute' => 'link',
					'format' => 'ntext',
					'visible' => true,
				],
				[
					'attribute' => 'icon',
					'visible' => false,
				],
				[
					'attribute' => 'order',
					'visible' => false,
				],
				[
					'attribute' => 'target',
					'visible' => false,
				],
				[
					'attribute' => 'params',
					'format' => 'ntext',
					'visible' => false,
				],
				[
					'attribute' => 'description',
					'format' => 'ntext',
					'visible' => false,
				],
				[
					'attribute' => 'active',
					'visible' => false,
					'value' => function($model){
						return Yii::$app->strings::getCondition($model->active);
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => Yii::$app->strings::getCondition(),
					'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'created_by',
					'visible' => false,
					'value' => function($model){
						return $model->creadoPor->username;
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => Users::getData(),
					'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'created_at',
					'visible' => false,
					'filterType' => GridView::FILTER_DATE,
					'filterInputOptions' => ['id' => 'created_at_grid'],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'updated_by',
					'visible' => false,
					'value' => function($model){
						return $model->modificadoPor->username;
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => Users::getData(),
					'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'updated_at',
					'visible' => false,
					'filterType' => GridView::FILTER_DATE,
					'filterInputOptions' => ['id' => 'updated_at_grid'],
					'hiddenFromExport' => true,
				],
                [
                    'class' => 'ticmakers\core\base\ActionColumn',
                    
                ],
            ],
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'toolbar' => [
                    '{toggleData}',
                    [
                        'content' =>
                        	Yii::$app->ui::btnNew('') . ' '
                            .Yii::$app->ui::btnSearch('') . ' '
                            .Yii::$app->ui::btnRefresh('')
                    ],
                    '{dynagrid}',
                    '{export}',
                ],
                'pjax' => true,
                'options' => ['id' => 'gridview-menu-items'],
				'panelHeadingTemplate' => '<h3>' . (Yii::t($searchModel->module->id, $this->title)) . '</h3>'
            ],
            'options' => ['id' => 'dynagrid-menu-items'],
        ]) ?>
        </div>
<?php Modal::begin([
    'id'      => 'search-modal',
    'header'  => Yii::t($searchModel->module->id, 'Búsqueda Avanzada'),
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t($searchModel->module->id, 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t($searchModel->module->id, 'Buscar'),  ['form' => 'menu-items-search-form']),
    'options' => [ 'style' => 'display: none;', 'tabindex' => false ]
]);
echo $this->render('menu-items/_search', ['model' => $searchModel]);
Modal::end();
?>
