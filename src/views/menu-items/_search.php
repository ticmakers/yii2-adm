<?php

use ticmakers\adm\models\base\Users;

use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\searchs\MenuItems */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="menu-items-search">
    <?php $form = ActiveForm::begin([
		'id' => 'menu-items-search-form',
		'action' => ['index'],
		'method' => 'get',
	]); ?>

    <div class="row">        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput(['tabindex' => 2, 'id' => 'name_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'parent_menu_id', ['help' => '', 'popover' => $model->getHelp('parent_menu_id')])->widget(
				\kartik\widgets\Select2::classname(),
				[
					'data' => ticmakers\adm\models\base\MenuItems::getData(),
					'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 3, 'id' => 'parent_menu_id_search'],
				]
			) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

            <?= $form->field($model, 'internal', ['help' => '', 'popover' => $model->getHelp('internal')])->widget(
				\kartik\widgets\Select2::classname(),
				[
					'data' => Yii::$app->strings::getCondition(),
					'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 4, 'id' => 'internal_search'],
				]
			) ?>
            <?= $form->field($model, 'internal', ['help' => '', 'popover' => $model->getHelp('internal')])->textInput(['tabindex' => 4]) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'route_id', ['help' => '', 'popover' => $model->getHelp('route_id')])->widget(
				\kartik\widgets\Select2::classname(),
				[
					'data' => ticmakers\adm\models\base\AuthItem::getData(),
					'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 5, 'id' => 'route_id_search'],
				]
			) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'link', ['help' => '', 'popover' => $model->getHelp('link')])->textInput(['tabindex' => 6, 'id' => 'link_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'icon', ['help' => '', 'popover' => $model->getHelp('icon')])->textInput(['tabindex' => 7, 'id' => 'icon_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'order', ['help' => '', 'popover' => $model->getHelp('order')])->textInput(['tabindex' => 8, 'id' => 'order_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'target', ['help' => '', 'popover' => $model->getHelp('target')])->textInput(['tabindex' => 9, 'id' => 'target_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'params', ['help' => '', 'popover' => $model->getHelp('params')])->textInput(['tabindex' => 10, 'id' => 'params_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'description', ['help' => '', 'popover' => $model->getHelp('description')])->textInput(['tabindex' => 11, 'id' => 'description_search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'active', ['help' => '', 'popover' => $model->getHelp('active')])->widget(
				\kartik\widgets\Select2::classname(),
				[
					'data' => Yii::$app->strings::getCondition(),
					'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 12, 'id' => 'active_search'],
				]
			) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'created_by', ['help' => '', 'popover' => $model->getHelp('created_by')])->widget(
				\kartik\widgets\Select2::classname(),
				['data' => Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 13, 'id' => 'created_by_search']]
			) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'created_at', ['help' => '', 'popover' => $model->getHelp('created_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 14, 'id' => 'created_at_search']]) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'updated_by', ['help' => '', 'popover' => $model->getHelp('updated_by')])->widget(
				\kartik\widgets\Select2::classname(),
				['data' => Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 15, 'id' => 'updated_by_search']]
			) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'updated_at', ['help' => '', 'popover' => $model->getHelp('updated_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 16, 'id' => 'updated_at_search']]) ?>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'menu_id')->hiddenInput()->label(false) ?>

        </div>

    </div>    <?php ActiveForm::end(); ?>
</div> 