<?php

use ticmakers\adm\helpers\Strings;
use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\MenuItems */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="menu-items-form">
    <?php $form = ActiveForm::begin([
        'id' => 'menu-items-form',
    ]); ?>
    <?= $form->field($model, 'menu_id')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput(['maxlength' => true, 'tabindex' => 2]) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'parent_menu_id', ['help' => '', 'popover' => $model->getHelp('parent_menu_id')])->widget(
                \kartik\widgets\Select2::classname(),
                [
                    'data' => ticmakers\adm\models\base\MenuItems::getData(),
                    'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 3],
                ]
            ) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'internal', ['help' => '', 'popover' => $model->getHelp('internal')])->widget(
                \kartik\widgets\Select2::classname(),
                [
                    'data' => Yii::$app->strings::getCondition(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty(), 
                        'tabindex' => 4,
                        'id'=>'internal-form'
                    ],
                ]
            ) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'route_id', ['help' => '', 'popover' => $model->getHelp('route_id')])->widget(
                \kartik\widgets\Select2::classname(),
                [
                    'data' => ticmakers\adm\models\base\AuthItem::getData(),
                    'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 5],
                ]
            ) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'order', ['help' => '', 'popover' => $model->getHelp('order')])->textInput(['tabindex' => 8]) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'icon', ['help' => '', 'popover' => $model->getHelp('icon')])->textInput(['maxlength' => true, 'tabindex' => 7]) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'link', ['help' => '', 'popover' => $model->getHelp('link')])->textInput(['tabindex' => 6]) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'target', ['help' => '', 'popover' => $model->getHelp('target')])->textInput(['maxlength' => true, 'tabindex' => 9]) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'params', ['help' => '', 'popover' => $model->getHelp('params')])->textInput(['tabindex' => 10]) ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'description', ['help' => '', 'popover' => $model->getHelp('description')])->textarea(['rows' => 2, 'tabindex' => 11]) ?>

        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div> 