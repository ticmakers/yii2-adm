<?php
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\MenuItems */

 $this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Menu Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-items-view">

    <p>
        <?=  Yii::$app->ui::btnUpdate(null, ['update', 'id' => $model->menu_item_id]); ?>
        <?=  Yii::$app->ui::btnDelete(null, ['delete', 'id' => $model->menu_item_id]); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'menu_item_id',
            [
                'attribute' => 'menu_id',
                'value'     => function ($model, $widget){
                    $name = $model->menu->getNameFromRelations();

                    return $model->menu->$name;
                },
            ],
            'name',
            [
                'attribute' => 'parent_menu_id',
                'value'     => function ($model, $widget){
                    $name = $model->parentMenu->getNameFromRelations();

                    return $model->parentMenu->$name;
                },
            ],
            'internal',
            [
                'attribute' => 'route_id',
                'value'     => function ($model, $widget){
                    $name = $model->route->getNameFromRelations();

                    return $model->route->$name;
                },
            ],
            'link:ntext',
            'icon',
            'order',
            'target',
            'params:ntext',
            'description:ntext',
            [
                'attribute' => 'active',
                'value'     => Yii::$app->strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
