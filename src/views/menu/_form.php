<?php
use ticmakers\core\widgets\Tabs;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Menus */
/* @var $form ticmakers\core\widgets\ActiveForm */

$tabActive = Yii::$app->request->get('tab', 1);
?>
<div class="menus-form">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => Yii::t($model->module->id, 'Information'),
                'content' => $this->render('1_informacion', ['model' => $model]),
                'active' => $tabActive == 1,
            ],
            [
                'label' => Yii::t($model->module->id, 'Menu elements'),
                'content' => $model->isNewRecord ? '' : $this->render('2_menu_elements', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'linkOptions' => [
                    'class' => $model->isNewRecord ? 'disabled' : '',
                ],
                'active' => $tabActive == 2,
            ],
        ],
    ]);
    ?>

    <div class="form-group text-right">
        <?= Yii::$app->ui::btnCancel(); ?>
        <?= Yii::$app->ui::btnSend(Yii::t($model->module->id, 'Save and continue'), ['form' => 'menus-form']); ?>
    </div>

</div> 