<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Menus */

$this->title = Yii::t($model->module->id, 'Editar') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->menu_id]];
$this->params['breadcrumbs'][] = Yii::t($model->module->id, 'Editar');
?>
<div class="menus-update">
    <?php echo $this->render('_form', [
    'model' => $model,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
]) ?>
</div>