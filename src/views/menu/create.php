<?php

use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Menus */

$breadcrumbs = [];
$this->title = Yii::t($model->module->id, 'Create');
$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="menus-create">
    <?php echo $this->render('_form', [
    'model' => $model,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
]) ?>
</div>