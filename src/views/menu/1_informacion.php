<?php

use ticmakers\core\widgets\ActiveForm;
use ticmakers\adm\models\base\Menus;
use ticmakers\adm\helpers\Strings;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Menus */
/* @var $form ticmakers\core\widgets\ActiveForm */

$form = ActiveForm::begin([
    'id' => 'menus-form',
]);
?><div class="row">

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput(['maxlength' => true, 'tabindex' => 1]) ?>

    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model, 'type', ['help' => '', 'popover' => $model->getHelp('type')])->widget(
            \kartik\widgets\Select2::classname(),
            [
                'data' => Menus::getTypes(),
                'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 3],
            ]
        ) ?>

    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model, 'position', ['help' => '', 'popover' => $model->getHelp('position')])->textInput(['maxlength' => true, 'tabindex' => 4]) ?>

    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model, 'description', ['help' => '', 'popover' => $model->getHelp('description')])->textarea(['rows' => 6, 'tabindex' => 2]) ?>

    </div></div><?php ActiveForm::end(); ?> 