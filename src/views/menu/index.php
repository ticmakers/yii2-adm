<?php

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use ticmakers\adm\models\base\Users;
use ticmakers\core\base\Modal;
use yii\helpers\ArrayHelper;
use richardfan\sortable\SortableGridView;
use yii\helpers\Url;
use ticmakers\adm\models\base\Menus;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\adm\models\searchs\Menus */
/* @var $dataProvider yii\data\ActiveDataProvider */

$breadcrumbs = [];
$this->title = Yii::t($searchModel->module->id, 'Menus');
$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'][] = ['label' => Yii::t($searchModel->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menus-index">
    <?php echo DynaGrid::widget([
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'menu_id',
                'visible' => false,
            ],
            [
                'attribute' => 'name',
                'visible' => true,
            ],
            [
                'attribute' => 'type',
                'visible' => true,
                'value' => function ($model) {
                    return Menus::getTypes($model->type);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Menus::getTypes(),
                'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
                'hiddenFromExport' => true,
            ],
            [
                'attribute' => 'description',
                'format' => 'ntext',
                'visible' => true,
            ],
            [
                'attribute' => 'position',
                'visible' => false,
            ],
            [
                'attribute' => 'active',
                'visible' => false,
                'value' => function ($model) {
                    return Yii::$app->strings::getCondition($model->active);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Yii::$app->strings::getCondition(),
                'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
                'hiddenFromExport' => true,
            ],
            [
                'attribute' => 'created_by',
                'visible' => false,
                'value' => function ($model) {
                    return $model->creadoPor->username;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Users::getData(),
                'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
                'hiddenFromExport' => true,
            ],
            [
                'attribute' => 'created_at',
                'visible' => false,
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['id' => 'created_at_grid'],
                'hiddenFromExport' => true,
            ],
            [
                'attribute' => 'updated_by',
                'visible' => false,
                'value' => function ($model) {
                    return $model->modificadoPor->username;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Users::getData(),
                'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
                'hiddenFromExport' => true,
            ],
            [
                'attribute' => 'updated_at',
                'visible' => false,
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['id' => 'updated_at_grid'],
                'hiddenFromExport' => true,
            ],
            [
                'class' => 'ticmakers\core\base\ActionColumn',

            ],
        ],
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'toolbar' => [
                '{toggleData}',
                [
                    'content' =>
                    Yii::$app->ui::btnNew('') . ' '
                        . Yii::$app->ui::btnSearch('') . ' '
                        . Yii::$app->ui::btnRefresh(''),
                ],
            ],
            'pjax' => true,
            'options' => ['id' => 'gridview-menus'],
            'panelHeadingTemplate' => '<h3>' . (Yii::t($searchModel->module->id, $this->title)) . '</h3>'
        ],
        'options' => ['id' => 'dynagrid-menus'],
    ]) ?>
</div>
<?php Modal::begin([
    'id' => 'search-modal',
    'header' => Yii::t('app', 'Búsqueda Avanzada'),
    'size' => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t('app', 'Buscar'), ['form' => 'menus-search-form']),
    'options' => ['style' => 'display: none;', 'tabindex' => false],
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
?> 