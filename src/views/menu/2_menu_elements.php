<?php

use yii\helpers\Url;
use ticmakers\core\base\Modal;/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Menus */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<?= \ticmakers\sortablegridview\SortableGridView::widget([
    'columns' => [
        [
            'attribute' => 'name',
            'value' => function ($model) {
                if (empty($model->parent_menu_id)) {
                    return Yii::$app->html::tag('div', Yii::$app->html::tag('span', '', ['class' => 'fa fa-bars']) . ' ' . $model->name);
                } else {
                    return Yii::$app->html::tag('div', "&nbsp&nbsp&nbsp&nbsp" . Yii::$app->html::tag('span', '', ['class' => 'fa fa-arrow-right']) . ' ' . $model->name);
                }
            },
            'format' => 'raw',
            'visible' => true,
        ],
        [
            'class' => 'ticmakers\core\base\ActionColumn',
            'template' => "{roles} {update} {delete} {restore}",
            'isModal' => true,
            'dynaModalId' => 'menu-items',
            'buttons' => [
                'roles' => function ($url, $model, $key) {
                    return Yii::$app->html::a(Yii::$app->html::tag('span', '', ['class' => "fa fa-link"]), $url, ['onclick' => "openModalGrid(this, 'menu-items', 'update'); return false;"]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                return Url::to(['menu-items/' . $action, 'id' => $model->menu_item_id]);
            },
        ],
    ],
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'toolbar' => [
            '{toggleData}',
            [
                'content' =>
                Yii::$app->ui::btnNew('', ['menu-items/create-item', 'id' => $model->menu_id], ['onClick' => "openModalGrid(this, 'menu-items', 'create'); return false;"]) . ' '
                    . Yii::$app->ui::btnSearch('') . ' '
                    . Yii::$app->ui::btnRefresh('', ['menu/update', 'id' => $model->menu_id, 'tab' => 2]),
            ],
        ],
        'pjax' => true,
        'options' => ['id' => 'gridview-menu-items'],
    ],
    'options' => ['id' => 'dynagrid-menu-items'],
    'sortUrl' => Url::to(['sortItem']),
]); ?>

<?php Modal::begin([
    'id'      => 'search-modal',
    'header'  =>  Yii::t('app', 'Búsqueda Avanzada'),
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t('app', 'Buscar'),  ['form' => 'menu-items-search-form']),
    'options' => ['style' => 'display: none;', 'tabindex' => false]
]);
echo $this->render('../menu-items/_search', ['model' => $searchModel]);
Modal::end();
?>

<?php Modal::begin([
    'id'            => 'menu-items-modal',
    'header'        => 'Items',
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModalMessage(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(null,  ['form' => 'menu-items-form']),
    'closeButton' => ['class' => 'close confirm-close'],
    'options'       => ['style' => 'display: none;', 'tabindex' => false],
]);

Modal::end();
?> 