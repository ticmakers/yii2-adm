<?php

use ticmakers\adm\helpers\Strings;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Menus */

 $this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menus-view">

    <p>
        <?=  Yii::$app->ui::btnUpdate(null, ['update', 'id' => $model->menu_id]); ?>
        <?=  Yii::$app->ui::btnDelete(null, ['delete', 'id' => $model->menu_id]); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'menu_id',
            'name',
            'description:ntext',
            'type',
            'position',
            [
                'attribute' => 'active',
                'value'     => Yii::$app->strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                // 'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                // 'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
