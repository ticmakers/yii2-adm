<?php

use ticmakers\core\widgets\ActiveForm;

use ticmakers\adm\models\base\Users;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\searchs\Menus */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="menus-search">
    <?php $form = ActiveForm::begin([
        'id'     => 'menus-search-form',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput(['tabindex' => 1, 'id' => 'name_sea rch']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'description', ['help' => '', 'popover' => $model->getHelp('description')])->textInput(['tabindex' => 2, 'id' => 'descript ion_search'])  ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'type', ['help' => '', 'popover' => $model->getHelp('type')])->textInput(['tabindex' => 3, 'id' => 'type_ search']) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'position', ['help' => '', 'popover' => $model->getHelp('position')])->textInput(['tabindex' => 4, 'id' => 'positio n_search']) ?>

        </div>
        <div class="col-lg-6 col
                -md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'active', ['help' => '', 'popover' => $model->getHelp('active')])->widget(
                \kartik\widgets\Select2::classname(),
                [
                    'data' => Yii::$app->strings::getCondition(),
                    'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 5, 'id' => 'active_search'],
                ]
            ) ?>

        </div>
        <div class="col-lg-6 col-md-6 co
                l-sm-6 col-xs-12">
            <?= $form->field($model, 'created_by', ['help' => '', 'popover' => $model->getHelp('created_by')])->widget(
                \kartik\widgets\Select2::classname(),
                ['data' => Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 6, 'id' => 'created_by_search']]
            ) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'created_at', ['help' => '', 'popover' => $model->getHelp('created_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 7, 'id' => 'created_at_sea rch']]) ?>

        </div>
        <div class="col-lg-6 col-md-6 co
                l-sm-6 col-xs-12">
            <?= $form->field($model, 'updated_by', ['help' => '', 'popover' => $model->getHelp('updated_by')])->widget(
                \kartik\widgets\Select2::classname(),
                ['data' => Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 8, 'id' => 'updated_by_search']]
            ) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'updated_at', ['help' => '', 'popover' => $model->getHelp('updated_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 9, 'id' => 'updated_at_search']]) ?>

        </div>

    </div>    <?php ActiveForm::end(); ?>
</div> 