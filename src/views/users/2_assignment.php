<?php
/*
use mdm\admin\AnimateAsset; */

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\YiiAsset;
use ticmakers\adm\models\base\AuthItem;
/* @var $this yii\web\View */
/* @var $model mdm\admin\models\Assignment */
/* @var $fullnameField string */

YiiAsset::register($this);

$items = Json::htmlEncode([
    'roles' => $modelItems,
]);

$this->registerJs("var _items = {$items};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
?>
<div class="assignment-index">

    <div class="row">
        <div class="col-sm-5">
            <input class="form-control search" data-target="available" placeholder="<?= Yii::t($model->module->id, 'Search for available'); ?>">
            <select id="available-list" multiple size="20" class="form-control list" data-target="available">
            </select>
        </div>

        <div class="col-sm-1">
            <br><br>
            <?= Yii::$app->html::a('&gt;&gt;' . $animateIcon, ['auth-assignment/assign', 'id' => $model->user_id], [
                'class' => 'btn btn-success btn-assign',
                'data-target' => 'available',
                'title' => Yii::t($model->module->id, 'Assign'),
            ]); ?><br><br>
            <?= Yii::$app->html::a('&lt;&lt;' . $animateIcon, ['auth-assignment/revoke', 'id' => $model->user_id], [
                'class' => 'btn btn-danger btn-assign',
                'data-target' => 'assigned',
                'title' => Yii::t($model->module->id, 'Remove'),
            ]); ?>
        </div>

        <div class="col-sm-5">
            <input class="form-control search" data-target="assigned" placeholder="<?= Yii::t($model->module->id, 'Search for assigned'); ?>">
            <select id="assigned-list" multiple size="20" class="form-control list" data-target="assigned">
            </select>
        </div>
    </div>
</div>
<br>
<div class="form-group text-right">
    <?= Yii::$app->ui::btnCancel(Yii::t($model->module->id, 'Cancel')); ?>
    <?= Yii::$app->html::button(
        '<i class="fa fa-arrow-left"></i> ' .
            Yii::t($model->module->id, 'Previous'),
        ['class' => 'btn btn-info', 'onclick' => "$('a[href=\"#w0-tab0\"]').click();"]
    ); ?>
    <?= Yii::$app->ui::btnSend(Yii::t($model->module->id, 'Save'), ['form' => 'users-form']); ?>
</div>

<?php

$this->registerCss('
.glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -ms-animation: spin .7s infinite linear;
    -webkit-animation: spinw .7s infinite linear;
    -moz-animation: spinm .7s infinite linear;
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
  
@-webkit-keyframes spinw {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@-moz-keyframes spinm {
    from { -moz-transform: rotate(0deg);}
    to { -moz-transform: rotate(360deg);}
}');

?>