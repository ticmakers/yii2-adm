<?php

use ticmakers\core\widgets\ActiveForm;
use kartik\widgets\{Select2, DatePicker};
use ticmakers\adm\helpers\Strings;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\searchs\Users */
/* @var $form ticmakers\widgets\ActiveForm */
?>

<div class="users-search">
    <?php $form = ActiveForm::begin([
        'id'     => 'users-search-form',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'username',['help' => '','popover' => $model->getHelp('username')])->textInput(['tabindex' => 1, 'id' => 'username_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'email',['help' => '','popover' => $model->getHelp('email')])->textInput(['tabindex' => 2, 'id' => 'email_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'password_hash',['help' => '','popover' => $model->getHelp('password_hash')])->textInput(['tabindex' => 3, 'id' => 'password_hash_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'auth_key',['help' => '','popover' => $model->getHelp('auth_key')])->textInput(['tabindex' => 4, 'id' => 'auth_key_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'password_reset_token',['help' => '','popover' => $model->getHelp('password_reset_token')])->textInput(['tabindex' => 5, 'id' => 'password_reset_token_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'email_key',['help' => '','popover' => $model->getHelp('email_key')])->textInput(['tabindex' => 6, 'id' => 'email_key_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'confirmed_email',['help' => '','popover' => $model->getHelp('confirmed_email')])->widget(Select2::classname(),
					[
						'data' => Yii::$app->strings::getCondition(),
						'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 7, 'id' => 'confirmed_email_search'],
					]
                ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'blocked',['help' => '','popover' => $model->getHelp('blocked')])->widget(Select2::classname(),
					[
						'data' => Yii::$app->strings::getCondition(),
						'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 8, 'id' => 'blocked_search'],
					]
                )?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'accepted_terms',['help' => '','popover' => $model->getHelp('accepted_terms')])->widget(Select2::classname(),
                        [
                            'data' => Yii::$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 9, 'id' => 'accepted_terms_search'],
                        ]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'active',['help' => '','popover' => $model->getHelp('active')])->widget(Select2::classname(),
                        [
                            'data' => Yii::$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 10, 'id' => 'active_search'],
                        ]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_by',['help' => '','popover' => $model->getHelp('created_by')])->widget(Select2::classname(),
                        ['data' => ticmakers\adm\models\base\Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 11, 'id' => 'created_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->widget(DatePicker::classname(), ['options' => ['tabindex' => 12, 'id' => 'created_at_search']]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->widget(Select2::classname(),
                        ['data' => ticmakers\adm\models\base\Users::getData(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 13, 'id' => 'updated_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->widget(DatePicker::classname(), ['options' => ['tabindex' => 12, 'id' => 'updated_at_search']]) ?>

			</div>

    </div>    <?php ActiveForm::end(); ?>
</div>
