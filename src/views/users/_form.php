<?php

use ticmakers\core\widgets\Tabs;
use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Users */
/* @var $form ticmakers\widgets\ActiveForm */
$modelAssignment = new ticmakers\adm\models\base\AuthItem();
$form = ActiveForm::begin([
    'id' => 'users-form',
]);
?>
<div class="users-form">
    <?= Tabs::widget([
        'items' => [
            [
                'label'   => Yii::t($model->module->id, 'Information'),
                'content' => $this->render('1_information', ['form' => $form, 'model' => $model]),
                'active'  => true
            ],
            [
                'label'   => Yii::t($model->module->id, 'Assignments'),
                'content' => $this->render('2_assignment', ['model' => $model, 'modelItems' => !$model->isNewRecord ? $modelItems: []]),
                'linkOptions' => ['class' => $model->isNewRecord ? 'disabled' : ''],
            ],
        ],
    ]);
    ?>

    <?php ActiveForm::end(); ?>
</div>
