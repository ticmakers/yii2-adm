<?php
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Users */

$breadcrumbs = [];
$this->title = Yii::t( $model->module->id, 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t( $model->module->id, 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
