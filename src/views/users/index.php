<?php
use kartik\dynagrid\DynaGrid;
use ticmakers\core\base\Modal;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\adm\models\searchs\Users */
/* @var $dataProvider yii\data\ActiveDataProvider */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($searchModel->module->id, 'Users');
$this->params['breadcrumbs'][] = ['label' => Yii::t($searchModel->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-index">
        <?php echo DynaGrid::widget([
            'columns' => [
                ['class'=>'kartik\grid\SerialColumn'],
                				[
					'attribute' => 'user_id',
					'visible' => false,
				],
				[
					'attribute' => 'username',
					'visible' => true,
				],
				[
					'attribute' => 'email',
					'format' => 'email',
					'visible' => true,
				],
				[
					'attribute' => 'password_hash',
					'format' => 'ntext',
					'visible' => false,
				],
				[
					'attribute' => 'auth_key',
					'visible' => false,
				],
				[
					'attribute' => 'password_reset_token',
					'format' => 'ntext',
					'visible' => false,
				],
				[
					'attribute' => 'email_key',
					'format' => 'ntext',
					'visible' => false,
				],
				[
					'attribute' => 'confirmed_email',
					'format' => 'email',
					'visible' => false,
				],
				[
					'attribute' => 'blocked',
					'visible' => false,
				],
				[
					'attribute' => 'accepted_terms',
					'visible' => false,
				],
				[
					'attribute' => 'active',
					'visible' => false,
					'value' => function($model){
						return Yii::$app->strings::getCondition($model->active);
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => Yii::$app->strings::getCondition(),
					'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'created_by',
					'visible' => false,
					'value' => function($model){
						return $model->creadoPor->username;
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => ticmakers\adm\models\base\Users::getData(),
					'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'created_at',
					'visible' => true,
					'filterType' => GridView::FILTER_DATE,
					'filterInputOptions' => ['id' => 'created_at_grid'],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'updated_by',
					'visible' => false,
					'value' => function($model){
						return $model->modificadoPor->username;
					},
					'filterType' => GridView::FILTER_SELECT2,
					'filter' => ticmakers\adm\models\base\Users::getData(),
					'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
					'hiddenFromExport' => true,
				],
				[
					'attribute' => 'updated_at',
					'visible' => false,
				],
                [
                    'class' => 'ticmakers\core\base\ActionColumn',
                    
                ],
            ],
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'toolbar' => [
                    '{toggleData}',
                    [
                        'content' =>
                        	Yii::$app->ui::btnNew('') . ' '
                            .Yii::$app->ui::btnSearch('') . ' '
                            .Yii::$app->ui::btnRefresh('')
                    ]
                ],
                'pjax' => true,
                'options' => ['id' => 'gridview-users'],
            ],
            'options' => ['id' => 'dynagrid-users'],
        ]) ?>
        </div>
<?php Modal::begin([
    'id'      => 'search-modal',
    'header'  => Yii::t($searchModel->module->id, 'Búsqueda Avanzada'),
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t($searchModel->module->id, 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t($searchModel->module->id, 'Buscar'),  ['form' => 'users-search-form']),
    'options' => [ 'style' => 'display: none;', 'tabindex' => false ]
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
?>
