<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Users */
/* @var $form ticmakers\widgets\ActiveForm */
?>

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'username', ['help' => '', 'popover' => $model->getHelp('username')])->textInput(['maxlength' => true, 'tabindex' => 1]) ?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'email', ['help' => '', 'popover' => $model->getHelp('email')])->textInput(['maxlength' => true, 'tabindex' => 2]) ?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'password', ['help' => '', 'popover' => $model->getHelp('password')])->passwordInput(['maxlength' => true, 'tabindex' => 3]) ?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'confirmPassword', ['help' => '', 'popover' => $model->getHelp('confirmPassword')])->passwordInput(['maxlength' => true, 'tabindex' => 4]) ?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'confirmed_email', ['help' => '', 'popover' => $model->getHelp('confirmed_email')])->inline(true)->radioList(['Y' => Yii::t($model->module->id, 'Yes'), 'N' => Yii::t($model->module->id, 'No')], ['tabindex' => 5]) ?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?= $form->field($model, 'blocked', ['help' => '', 'popover' => $model->getHelp('blocked')])->inline(true)->radioList(['Y' => Yii::t($model->module->id, 'Yes'), 'N' => Yii::t($model->module->id, 'No')], ['tabindex' => 6]) ?>
	</div>

</div>
<br>
<div class="form-group text-right">
	<?= Yii::$app->ui::btnCancel(Yii::t($model->module->id, 'Cancel')); ?>
	<?= !$model->isNewRecord ? Yii::$app->html::button(
		'<i class="fa fa-arrow-right"></i> ' .
			Yii::t($model->module->id, 'Next'),
		['class' => 'btn btn-info', 'onclick' => "$('a[href=\"#w0-tab1\"]').click();"]
	) : ''; ?>
	<?= Yii::$app->ui::btnSend(Yii::t($model->module->id, 'Save'), ['form' => 'users-form']); ?>
</div>