<?php
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Users */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t( $model->module->id, 'Update') . ': ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Rbac'), 'url' => ['/adm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t( $model->module->id, 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t( $model->module->id, 'Update');
$this->params['breadcrumbs'][] = ['label' => $model->username];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
?>
<div class="users-update">
    <?php echo $this->render('_form', [
        'model' => $model,
        'modelItems' => $modelItems
    ]) ?>
</div>
