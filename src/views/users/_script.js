function updateItems(roles) {   
    _items.roles.available = roles.items.available;
    _items.roles.assigned = roles.items.assigned;
    search('available');
    search('assigned');
}

function getTypeName(type){
    let name = ''
    switch(type){
        case 'ROU':
            name = 'route';
        break;                
        case 'ROL':
            name = 'role'
        break;
        case 'PER':
            name = 'permission'
        break;
    }
    return name;
}

$('.btn-assign').click(function () {
    var $this = $(this);
    var target = $this.data('target');
    var items = $('select.list[data-target="' + target + '"]').val();

    if (items && items.length) {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post($this.attr('href'), {items: items}, function (roles) {
            updateItems(roles);
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});

$('.search[data-target]').keyup(function () {
    search($(this).data('target'));
}); 

function search(target) {
    var $list = $('select.list[data-target="' + target + '"]');
    $list.html('');
    var q = $('.search[data-target="' + target + '"]').val().toLowerCase();
    var groups = {
        role: [$('<optgroup label="Roles">'), false],
        permission: [$('<optgroup label="Permission">'), false],
        route: [$('<optgroup label="Routes">'), false],
    };
    $.each(_items.roles[target], function (itemName, group) {
        
        let currentName = group.name.toLowerCase();
        if (currentName.indexOf(q) >= 0) {
            let item = getTypeName(group.type);
            $('<option>').text(group.name).val(group.name).appendTo(groups[item][0]);
            groups[item][1] = true;
        }
    });
    $.each(groups, function () {
        if (this[1]) {
            $list.append(this[0]);
        }
    });

} 
$('i.glyphicon-refresh-animate').hide();
search('available');
search('assigned');
