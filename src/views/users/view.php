<?php
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\Users */

$this->title = $model->user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">

    <p>
        <?=  Yii::$app->ui::btnUpdate(null, ['update', 'id' => $model->user_id]); ?>
        <?=  Yii::$app->ui::btnDelete(null, ['delete', 'id' => $model->user_id]); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'username',
            'email:email',
            'password_hash:ntext',
            'auth_key',
            'password_reset_token:ntext',
            'email_key:ntext',
            'confirmed_email:email',
            'blocked',
            'accepted_terms',
            [
                'attribute' => 'active',
                'value'     => ''/*  Yii::$app->strings::getCondition($model->active), */
            ],
            [
                'attribute' => 'created_by',
                'value'     => ''/* $model->creadoPor->username, */
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => ''/* $model->modificadoPor->username */,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
