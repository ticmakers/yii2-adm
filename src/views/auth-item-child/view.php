<?php

use ticmakers\helpers\Strings;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\base\AuthItemChild */

 $this->title = $model->auth_item_child_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Auth Item Children'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-child-view">

    <p>
        <?=  Yii::$app->ui::btnUpdate(null, ['update', 'id' => $model->auth_item_child_id]); ?>
        <?=  Yii::$app->ui::btnDelete(null, ['delete', 'id' => $model->auth_item_child_id]); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'auth_item_child_id',
            'parent',
            'child',
            [
                'attribute' => 'active',
                'value'     => Yii::$app->strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
