<?php

/* @var $this yii\web\View */
/* @var $model app\models\base\AuthItemChild */

$this->title = Yii::t($model->module->id, 'Update') . ': ' . $model->auth_item_child_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t($model->module->id, 'Update');
$this->params['breadcrumbs'][] = ['label' => $model->auth_item_child_id];
?>
<div class="auth-item-child-update">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
