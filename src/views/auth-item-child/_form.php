<?php

use ticmakers\core\widgets\Tabs;
use ticmakers\core\widgets\ActiveForm;/* @var $this yii\web\View */
/* @var $model app\models\base\AuthItemChild */
/* @var $form ticmakers\widgets\ActiveForm */

$form = ActiveForm::begin([
    'id' => 'auth-item-child-form',
]);
?>
<div class="auth-item-child-form">
    <?= Tabs::widget([
        'items' => [
            [
                'label'   => Yii::t($model->module->id, 'Information'),
                'content' => $this->render('1_informacion', ['form' => $form, 'model' => $model]),
                'active'  => true
            ],
        ],
    ]);
    ?>

    <div class="form-group text-right">
        <?=  Yii::$app->ui::btnCancel(); ?>
        <?=  Yii::$app->ui::btnSend(); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
