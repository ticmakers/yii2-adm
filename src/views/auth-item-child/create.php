<?php

/* @var $this yii\web\View */
/* @var $model app\models\base\AuthItemChild */

$this->title = Yii::t($model->module->id, 'Crear permissions');
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-child-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
