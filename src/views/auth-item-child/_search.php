<?php

use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchs\AuthItemChild */
/* @var $form ticmakers\widgets\ActiveForm */
?>

<div class="auth-item-child-search">
    <?php $form = ActiveForm::begin([
        'id'     => 'auth-item-child-search-form',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'parent',['help' => '','popover' => $model->getHelp('parent')])->textInput(['tabindex' => 1, 'id' => 'parent_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'child',['help' => '','popover' => $model->getHelp('child')])->textInput(['tabindex' => 2, 'id' => 'child_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'active',['help' => '','popover' => $model->getHelp('active')])->widget(\kartik\widgets\Select2::classname(),
                        [
                            'data' => Yii::$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 3, 'id' => 'active_search'],
                        ]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_by',['help' => '','popover' => $model->getHelp('created_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => [], 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 4, 'id' => 'created_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 5, 'id' => 'created_at_search']]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => [], 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 6, 'id' => 'updated_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 7, 'id' => 'updated_at_search']]) ?>

			</div>

    </div>    <?php ActiveForm::end(); ?>
</div>
