<?php

use yii\helpers\ArrayHelper;

$breadcrumbs = [];
$this->title = Yii::t(Yii::$app->controller->module->id, 'Rbac');
$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rbac-default-index">
    <div class="d-flex">
        <div class="col-2">
            <?=Yii::$app->html::a(Yii::t(Yii::$app->controller->module->id, 'Menu'), ['menu/index'])?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t(Yii::$app->controller->module->id, 'Users'), ['users/index']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t(Yii::$app->controller->module->id, 'Roles'), ['auth-item/index']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t(Yii::$app->controller->module->id, 'Permissions'), ['auth-item/permission']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t(Yii::$app->controller->module->id, 'Routes'), ['auth-item/routes']) ?>
        </div>
    </div>
</div >