<?php

use ticmakers\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\searchs\AuthAssignment */
/* @var $form ticmakers\widgets\ActiveForm */
?>

<div class="auth-assignment-search">
    <?php $form = ActiveForm::begin([
        'id'     => 'auth-assignment-search-form',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'item_name',['help' => '','popover' => $model->getHelp('item_name')])->textInput(['tabindex' => 1, 'id' => 'item_name_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'user_id',['help' => '','popover' => $model->getHelp('user_id')])->textInput(['tabindex' => 2, 'id' => 'user_id_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'active',['help' => '','popover' => $model->getHelp('active')])->widget(\kartik\widgets\Select2::classname(),
                        [
                            'data' => Yii::$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 3, 'id' => 'active_search'],
                        ]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_by',['help' => '','popover' => $model->getHelp('created_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Yii::$app->userHelper::getUsers(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 4, 'id' => 'created_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 5, 'id' => 'created_at_search']]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Yii::$app->userHelper::getUsers(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 6, 'id' => 'updated_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->textInput(['tabindex' => 7, 'id' => 'updated_at_search']) ?>

			</div>

    </div>    <?php ActiveForm::end(); ?>
</div>
