<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthAssignment */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($model->module->id, 'Editar') . ': ' . $model->auth_assignment_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Auth Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t($model->module->id, 'Editar');
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-update">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
