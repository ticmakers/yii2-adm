<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthAssignment */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($model->module->id, 'Crear Auth Assignment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Auth Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>