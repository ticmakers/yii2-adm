<?php

use kartik\dynagrid\DynaGrid;
use ticmakers\core\base\Modal;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\adm\models\searchs\AuthAssignment */
/* @var $dataProvider yii\data\ActiveDataProvider */

$breadcrumbs = [];
$this->params['breadcrumbs'] = [];
$this->title = Yii::t($searchModel->module->id, 'Auth Assignments');
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index">
	<?php echo DynaGrid::widget([
		'columns' => [
			['class' => 'kartik\grid\SerialColumn'],
			[
				'attribute' => 'auth_assignment_id',
				'visible' => false,
			],
			[
				'attribute' => 'item_name',
				'visible' => true,
			],
			[
				'attribute' => 'user_id',
				'visible' => true,
			],
			[
				'attribute' => 'active',
				'visible' => false,
				'value' => function ($model) {
					return Yii::$app->strings::getCondition($model->active);
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->strings::getCondition(),
				'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'created_by',
				'visible' => false,
				'value' => function ($model) {
					return $model->creadoPor->username;
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->userHelper::getUsers(),
				'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'created_at',
				'visible' => true,
				'filterType' => GridView::FILTER_DATE,
				'filterInputOptions' => ['id' => 'created_at_grid'],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'updated_by',
				'visible' => true,
				'value' => function ($model) {
					return $model->modificadoPor->username;
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->userHelper::getUsers(),
				'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'updated_at',
				'visible' => false,
			],
			[
				'class' => 'ticmakers\core\ActionColumn',

			],
		],
		'gridOptions' => [
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'toolbar' => [
				'{toggleData}',
				[
					'content' =>
					Yii::$app->ui::btnNew('') . ' '
						. Yii::$app->ui::btnSearch('') . ' '
						. Yii::$app->ui::btnRefresh('')
				],
				'{dynagrid}',
				'{export}',
			],
			'pjax' => true,
			'options' => ['id' => 'gridview-auth-assignment'],
		],
		'options' => ['id' => 'dynagrid-auth-assignment'],
	]) ?>
</div>
<?php Modal::begin([
	'id'      => 'search-modal',
	'header'  => '' . Yii::t($searchModel->module->id, 'Búsqueda Avanzada'),
	'size'    => Modal::SIZE_LARGE,
	'footer' => Yii::$app->ui::btnCloseModal(Yii::t($searchModel->module->id, 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t($searchModel->module->id, 'Buscar'),  ['form' => 'auth-assignment-search-form']),
	'options' => ['style' => 'display: none;', 'tabindex' => false]
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
?>