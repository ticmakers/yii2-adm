<?php

use ticmakers\widgets\Tabs;
use ticmakers\widgets\ActiveForm;/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthAssignment */
/* @var $form ticmakers\widgets\ActiveForm */

$form = ActiveForm::begin([
    'id' => 'auth-assignment-form',
]);
?>
<div class="auth-assignment-form">
    <?= Tabs::widget([
        'items' => [
            [
                'label'   => Yii::t($model->module->id, 'Información'),
                'content' => $this->render('1_informacion', ['form' => $form, 'model' => $model]),
                'active'  => true
            ],
        ],
    ]);
    ?>

    <div class="form-group text-right">
        <?=  Yii::$app->ui::btnCancel(); ?>
        <?=  Yii::$app->ui::btnSend(); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
