<?php

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthAssignment */
/* @var $form ticmakers\widgets\ActiveForm */
?>

<div class="row">
        
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'item_name',['help' => '','popover' => $model->getHelp('item_name')])->textInput(['maxlength' => true, 'tabindex'=>1]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'user_id',['help' => '','popover' => $model->getHelp('user_id')])->textInput(['tabindex'=>2]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->textInput(['tabindex'=>3]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->textInput(['tabindex'=>4]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->textInput(['tabindex'=>5]) ?>

			</div>

</div>
