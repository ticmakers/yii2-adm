<?php

use ticmakers\helpers\Strings;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model ticmakers\adm\models\base\AuthAssignment */

$this->title = $model->auth_assignment_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t($model->module->id, 'Auth Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-view">

    <p>
        <?= Yii::$app->ui::btnUpdate(null, ['update', 'id' => $model->auth_assignment_id]); ?>
        <?= Yii::$app->ui::btnDelete(null, ['delete', 'id' => $model->auth_assignment_id]); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'auth_assignment_id',
            'item_name',
            'user_id',
            [
                'attribute' => 'active',
                'value'     => Yii::$app->strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>