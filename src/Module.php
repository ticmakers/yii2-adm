<?php

namespace ticmakers\adm;

use Yii;


/**
 * adm module definition class
 */
class Module extends \ticmakers\core\base\Module
{

    /**
     * Permite establecer el tiempo de duración para la sesión en segundos
     *
     * @var [type]
     */
    public $timeExpiredSession;

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'ticmakers\adm\controllers';
    /**
     * Permite definir si los terminos y condiciones seran requridos al momento de registrar un usuario
     *      
     * @var array
     */
    public $useTermsAndConditions;
    /**
     * Configuración para el inicio de sesión por redes sociales.
     * 
     * @var array
     */

    /**
     * @todo Estas son constantes por lo tanto deben eliminarce y ser requeridas en la configuración del proyecto.
     */
    public $socialAuthConfig = [
        'firebaseUrl' => 'https://www.gstatic.com/firebasejs/5.8.6/firebase.js',
        'apiKey' => 'AIzaSyDxJLJLMwgre8mDCKqxGVqmn7KFEQ89am4',
        'authDomain' => 'yii2-adm.firebaseapp.com',
        'databaseURL' => 'https://yii2-adm.firebaseio.com',
        'projectId' => 'yii2-adm',
        'storageBucket' => 'yii2-adm.appspot.com',
        'messagingSenderId' => '117951071783'
    ];

    /**
     * Undocumented function
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        Yii::createObject(\ticmakers\adm\modules\api\Bootstrap::class)->bootstrap($this);
    }
}
