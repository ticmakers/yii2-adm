<?php

namespace ticmakers\adm;

use Yii;

use yii\base\Application;
use yii\base\BootstrapInterface;
use ticmakers\adm\models\base\AuthAssignment;
use ticmakers\adm\models\base\AuthItem;
use ticmakers\adm\models\base\AuthItemChild;
use ticmakers\adm\models\base\AuthRule;
use ticmakers\adm\models\base\MenuItems;
use ticmakers\adm\models\base\Menus;
use ticmakers\adm\models\base\RoleMenuItems;
use ticmakers\adm\models\base\SocialNetworks;
use ticmakers\adm\models\base\Users;

use ticmakers\adm\models\searchs\AuthAssignment as AuthAssignmentSearch;
use ticmakers\adm\models\searchs\AuthItem as AuthItemSearch;
use ticmakers\adm\models\searchs\AuthItemChild as AuthItemChildSearch;
use ticmakers\adm\models\searchs\MenuItems as MenuItemsSearch;
use ticmakers\adm\models\searchs\Menus as MenusSearch;
use ticmakers\adm\models\searchs\Users as UsersSearch;

use ticmakers\adm\models\forms\Login;

/**
 * Class Bootstrap
 * @package ticmakers\yii2-adm
 * @author Kevin Daniel Guzmán Delgadillo <kevin.guzman@ticmakers.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [
            'AuthAssignment' => AuthAssignment::class,
            'AuthItem' => AuthItem::class,
            'AuthItemChild' => AuthItemChild::class,
            'AuthRule' => AuthRule::class,
            'MenuItems' => MenuItems::class,
            'Menus' => Menus::class,
            'RoleMenuItems' => RoleMenuItems::class,
            'SocialNetworks' => SocialNetworks::class,
            'Users' => Users::class,
        ],
        'searchs' => [
            'AuthAssignment' => AuthAssignmentSearch::class,
            'AuthItem' => AuthItemSearch::class,
            'AuthItemChild' => AuthItemChildSearch::class,
            'MenuItems' => MenuItemsSearch::class,
            'Menus' => MenusSearch::class,
            'Users' => UsersSearch::class,
        ],
        'task' => [],
        'forms' => [
            'Login' => Login::class
        ],
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('adm')) {
            $app->setModule('adm', 'ticmakers\adm\Module');
        }

        $this->overrideViews($app, 'adm');
        $this->overrideModels($app, 'adm');

    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app, $moduleId)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getView()->theme->pathMap["@ticmakers/{$moduleId}/views"] = "@app/views/{$moduleId}";
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "ticmakers\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}
