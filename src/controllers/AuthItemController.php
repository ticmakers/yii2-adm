<?php

namespace ticmakers\adm\controllers;

use Yii;
use ticmakers\core\base\Controller;

use yii\web\Response;
use ticmakers\adm\models\base\AuthItemChild;
use ticmakers\adm\models\base\AuthItem;
use ticmakers\adm\models\searchs\AuthItem as AuthItemSearch;

/**
 * Controlador AuthItemController implementa las acciones para el CRUD de el modelo AuthItem.
 *
 * @package ticmakers\adm
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthItemController extends Controller
{
    public $modelClass = AuthItem::class;
    public $searchModelClass = AuthItemSearch::class;

    /**
     * Establecemos el tipo de permiso
     *
     * @param [type] $searchModel
     * @return void
     */
    public function beforeSearchIndex(&$searchModel)
    {
        $searchModel->type = AuthItem::TYPE_ROL;
    }

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = Yii::createObject($this->modelClass);

        if ($model->load(Yii::$app->request->post())) {
            $model->type = 'ROL';
            if ($model->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($model->module->id, 'It was created successfully'));
                return $this->redirect(['update', 'id' => $model->primaryKey]);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($model->module->id, 'There was an error trying to create.'));
            }
        }

        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelItems = [
            'available' => $model->getAvailableRoleItems($model->name),
            'assigned' => $model->getAssignedRoleItems($model->name)
        ];
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($model->module->id, 'It was updated successfully'));

                return $this->redirect(['index']);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($model->module->id, 'Ocurrió un error al intentar editar.'));
            }
        }

        return $this->render(
            'update',
            [
                'model' => $model,
                'modelItems' => $modelItems
            ]
        );
    }


      
     /**
     * Permite incativar un registro en el sistema
     *
     * @return void
     */
    public function actionDelete($id)
    {
        $modelObject = $this->findModel($id);
        $modelObject->{$this->modelClass::STATUS_COLUMN} = $this->modelClass::STATUS_INACTIVE;
        if ($modelObject->save(false, [$this->modelClass::STATUS_COLUMN])) {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($modelObject->module->id, 'It was eliminated successfully.'));
        } else {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($modelObject->module->id, 'There was an error trying to delete.'));
        }
        
        return $modelObject->type == $modelObject::TYPE_ROL ? $this->redirect('index') : $this->redirect('permission');
        
    }
    
    /**
     * Permite ativar un registro en el sistema
     *
     * @return void
     */
    public function actionRestore($id)
    {
        $modelObject = $this->findModel($id);
        $modelObject->{$this->modelClass::STATUS_COLUMN} = $this->modelClass::STATUS_ACTIVE;

        if ($modelObject->save(false, [$this->modelClass::STATUS_COLUMN])) {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($modelObject->module->id, 'It was restored successfully.'));
        } else {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($modelObject->module->id, 'There was an error trying to restore.'));
        }

        return $modelObject->type == $modelObject::TYPE_ROL ? $this->redirect('index') : $this->redirect('permission');
    }


    /**
     * Lista todos registros según el DataProvider.
     *
     * @return string
     */
    public function actionPermission()
    {
        $searchModel = Yii::createObject($this->searchModelClass);
        $searchModel->type = 'PER';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'permissions/index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
    /**
     * Permite visualizar el formulario para la creación de un permiso
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreatePermission()
    {
        $model = Yii::createObject($this->modelClass);
        if ($model->load(Yii::$app->request->post())) {
            $model->type = 'PER';
            if ($model->save()) {

                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($model->module->id, 'It was created successfully'));
                return $this->redirect(['auth-item/update-permission', 'id' => $model->primaryKey]);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($model->module->id, 'There was an error trying to create.'));
            }
        }

        return $this->render(
            'permissions/create',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdatePermission($id)
    {
        $model = $this->findModel($id);
        $modelRoutes = [
            'available' => $model->getAvailableRoutesPermission($model->name),
            'assigned' => $model->getAssignedRoutesPermission($model->name)
        ];
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($model->module->id, 'It was created successfully'));

                return $this->redirect(['auth-item/permission']);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($model->module->id, 'Ocurrió un error al intentar editar.'));
            }
        }

        return $this->render(
            'permissions/update',
            [
                'model' => $model,
                'modelRoutes' => $modelRoutes
            ]
        );
    }

    /**
     * Permite obtener el listado actualizado de rutas asignads y disponibles
     * 
     * 
     */
    public function actionRefresh()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Yii::createObject($this->modelClass);
        $response = [
            'routes' => [
                'available' => $model->getAvailableRoutes(),
                'assigned' => $model->getAssignedRoutes()
            ]
        ];
        return $response;
    }
    /**
     * Permite visualizar el formulario de creación y asignación de rutas.     * 
     * 
     * 
     */
    public function actionRoutes()
    {
        $model = Yii::createObject($this->modelClass);
        $modelRoutes = [
            'available' => $model->getAvailableRoutes(),
            'assigned' => $model->getAssignedRoutes()
        ];
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->render(
            'routes/_form',
            [
                'model' => $model,
                'modelRoutes' => $modelRoutes
            ]
        );
    }
    /**
     * Permite crear una nueva ruta
     * 
     * @return boolean
     */
    public function actionCreateRoute()
    {
        $route = Yii::$app->request->post('route');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $model = Yii::createObject($this->modelClass);

        $result = $model->createRoute($route);
        if ($result['status']) {
            $response = [
                'status' => true,
                'routes' => $result['routes'],
                'message' => Yii::t($model->module->id, $result['message'])
            ];
        } else {
            $response = [
                'status' => false,
                'message' => Yii::t($model->module->id, $result['message'])
            ];
        }
        return $response;
    }
    /**
     * Asigna los permisos o items seleccionados a determinado rol
     * 
     * @return array[]
     */
    public function actionAssign()
    {
        $modelAssign = Yii::createObject(AuthItemChild::class);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $parent = Yii::$app->request->post('parent');
        return $modelAssign->assignItemsToRoles($items, $parent);
    }
    /**
     * Remover los permisos o items de un rol determinado
     *   
     * @return array[] $response | listado de rutas actualizado 
     */
    public function actionRevoke()
    {
        $modelAssign = Yii::createObject(AuthItemChild::class);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $parent = Yii::$app->request->post('parent');
        return $modelAssign->revokeItemsToRoles($items, $parent);
    }

    /**
     * Asignar las rutas del sistema par almacenarlas en la bd
     * 
     * @return array []
     * 
     */
    public function actionAssignRoutes()
    {
        $modelAuth = Yii::createObject($this->modelClass);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $routes = Yii::$app->getRequest()->post('routes', []);

        return $modelAuth->assignRoutes($routes);
    }

    /**
     * Remover las rutas asignadas y almacenadas en la bd
     * 
     * @return array []
     */
    public function actionRevokeRoutes()
    {
        $modelAuth = Yii::createObject($this->modelClass);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $routes = Yii::$app->getRequest()->post('routes', []);
        return $modelAuth->revokeRoutes($routes);
    }
}
