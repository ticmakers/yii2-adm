<?php

namespace ticmakers\adm\controllers;

use Yii;
use ticmakers\core\base\Controller;
use yii\web\Response;
use ticmakers\adm\models\base\AuthAssignment as AuthAssignment;
use ticmakers\adm\models\searchs\AuthAssignment as AuthAssignmentSearch;

/**
 * Controlador AuthAssignmentController implementa las acciones para el CRUD de el modelo AuthAssignment.
 *
 * @package ticmakers\adm
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthAssignmentController extends Controller
{
    public $modelClass = AuthAssignment::class;
    public $searchModelClass = AuthAssignmentSearch::class;
    /**
     * Asignar rol(es) a un usuario y registrarlo
     * 
     * @param int $id | identificador del usuario  
     * @return array[] $roleList | listado de roles actualizado 
     */
    public function actionAssign($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $model = Yii::createObject($this->modelClass);
        return $model->assignItems($id, $items);
    }

    /**
     * Remover rol(es) a un usuario y registrarlo
     * 
     * @param int $id | identificador del usuario  
     * @return array[] $roleList | listado de roles actualizado 
     */
    public function actionRevoke($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $model = Yii::createObject($this->modelClass);
        return $model->revokeItems($id, $items);
    }
}
