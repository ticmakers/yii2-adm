<?php

namespace ticmakers\adm\controllers;

use Yii;
use ticmakers\core\base\Controller;
use yii\web\Response;
use ticmakers\adm\models\base\AuthItemChild;
use ticmakers\adm\models\searchs\AuthItemChild as AuthItemChildSearch;

/**
 * Controlador AuthItemChildController implementa las acciones para el CRUD de el modelo AuthItemChild.
 *
 * @package ticmakers/adm
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthItemChildController extends Controller
{
    public $modelClass = AuthItemChild::class;
    public $searchModelClass = AuthItemChildSearch::class;

    /**
     * Registrar rutas 
     *   
     * @return array[] $response | listado de rutas actualizado 
     */
    public function actionAssign()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $parent = Yii::$app->request->post('parent');
        $model = Yii::createObject($this->modelClass);
        return $model->assignRoutesPermission($items, $parent);
    }

    /**
     * Remover rol(es) a un usuario y registrarlo
     * 
     * @param int $id | identificador del usuario  
     * @return array[] $response | listado de rutas actualizado 
     */
    public function actionRevoke()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $parent = Yii::$app->request->post('parent');
        $model = Yii::createObject($this->modelClass);
        return $model->revokeRoutesPermission($items, $parent);
    }
}
