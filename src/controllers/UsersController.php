<?php

namespace ticmakers\adm\controllers;

use Yii;
use ticmakers\core\base\Controller;

use ticmakers\adm\models\base\AuthItem;
use ticmakers\adm\models\base\Users;
use ticmakers\adm\models\searchs\Users as UsersSearch;

/**
 * Controlador UsersController implementa las acciones para el CRUD de el modelo Users.
 *
 * @package ticmakers\adm
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 2.0.0
 */
class UsersController extends Controller
{
    public $modelClass = Users::class;
    public $searchModelClass = UsersSearch::class;
    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $modelInstance = Yii::createObject($this->modelClass);
        $modelInstance->scenario = $modelInstance::SCENARIO_CREATE;
        if ($modelInstance->load(Yii::$app->request->post())) {
            if ($modelInstance->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t(
                    $modelInstance->module->id,
                    'It was created successfully.'
                ));
                return $this->redirect(['update', 'id' => $modelInstance->primaryKey]);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($modelInstance->module->id, 'An error occurred while trying to create.'));
            }
        }
        return $this->render('create', [
            'model' => $modelInstance,
        ]);
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $modelInstance = $this->findModel($id);
        $modelInstance->scenario = $modelInstance::SCENARIO_UPDATE;
        $modelItems = [
            'available' => AuthItem::getAvailableItems($modelInstance->user_id),
            'assigned' => AuthItem::getAssignedItems($modelInstance->user_id)
        ];
        if ($modelInstance->load(Yii::$app->request->post())) {
            if ($modelInstance->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($modelInstance->module->id, 'It was successfully edited.'));
                return $this->redirect(['index']);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($modelInstance->module->id, 'An error occurred while trying to edit.'));
            }
        }

        return $this->render('update', [
            'model' => $modelInstance,
            'modelItems' => $modelItems
        ]);
    }
}
