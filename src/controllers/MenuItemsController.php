<?php

namespace ticmakers\adm\controllers;

use ticmakers\adm\models\base\RoleMenuItems;
use ticmakers\core\base\Controller;
use ticmakers\core\widgets\ActiveForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use ticmakers\adm\models\base\MenuItems;
use ticmakers\adm\models\searchs\MenuItems as MenuItemsSearch;
use ticmakers\core\actions\DeleteAction;

/**
 * Controlador MenuItemsController implementa las acciones para el CRUD de el modelo MenuItems.
 *
 * @package ticmakers/adm
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class MenuItemsController extends Controller
{
    public $isModal = true;
    public $modelClass = MenuItems::class;
    public $searchModelClass = MenuItemsSearch::class;

    /**
     * Realiza la eleminación de un registro
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $modelInstance = $this->findModel($id);
        $modelInstance->{$modelInstance::STATUS_COLUMN} = $modelInstance::STATUS_INACTIVE;
        if ($modelInstance->save(false, [$modelInstance::STATUS_COLUMN])) {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', 'It was successfully removed.'));
        } else {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'An error occurred while trying to delete.'));
        }
        return $this->redirect(['menu/update', 'id' => $modelInstance->menu_id]);
    }

    /**
     * Realiza la restauración de un registro
     * 
     * @return mixed
     */
    public function actionRestore($id)
    {
        $modelInstance = $this->findModel($id);
        $modelInstance->{$modelInstance::STATUS_COLUMN} = $modelInstance::STATUS_ACTIVE;
        if ($modelInstance->save(false, [$modelInstance::STATUS_COLUMN])) {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', 'It was successfully restore.'));
        } else {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'An error occurred while trying to restore.'));
        }
        return $this->redirect(['menu/update', 'id' => $modelInstance->menu_id]);
    }

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreateItem($id)
    {
        $model = Yii::createObject($this->modelClass);
        $model->menu_id = $id;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $res['state'] = 'success';
                $res['message'] = Yii::t('app', 'It was created successfully');
            } else {
                $res['state'] = 'error';
                $res['message'] = Yii::$app->html::errorSummary($model);
                $res['error'] = ActiveForm::validate($model);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_form',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function actionRoles($id)
    {

        $model = Yii::createObject(RoleMenuItems::class);
        $model->menu_item_id = $id;

        $model->roles = ArrayHelper::getColumn(RoleMenuItems::find()
            ->select('role_id')
            ->where([
                'menu_item_id' => $model->menu_item_id,
                RoleMenuItems::STATUS_COLUMN => RoleMenuItems::STATUS_ACTIVE,
            ])
            ->asArray()
            ->all(), 'role_id');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $result = true;

            RoleMenuItems::updateAll([
                RoleMenuItems::STATUS_COLUMN => RoleMenuItems::STATUS_INACTIVE,
            ], ['menu_item_id' => $model->menu_item_id]);

            foreach ($model->roles as $role) {
                $modelNew = RoleMenuItems::find()
                    ->where([
                        'role_id' => $role,
                        'menu_item_id' => $model->menu_item_id,
                    ])
                    ->one();

                if (!$modelNew) {
                    $modelNew = Yii::createObject([
                        'class' => RoleMenuItems::class,
                        'role_id' => $role,
                        'menu_item_id' => $model->menu_item_id
                    ]);
                }

                $modelNew->active = RoleMenuItems::STATUS_ACTIVE;
                $modelNew->roles = $model->roles;
                $modelNew->save();
            }

            $res['state'] = 'success';
            $res['message'] = Yii::t('app', 'It was created successfully');

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_roles',
                [
                    'model' => $model,
                ]
            );
        }
    }
}
