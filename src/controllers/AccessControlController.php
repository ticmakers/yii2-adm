<?php

namespace ticmakers\adm\controllers;

use ticmakers\core\base\Controller;
use ticmakers\adm\models\base\Users;
use ticmakers\adm\models\base\Users as UsersSearch;

class AccessControlController extends Controller
{

    public $modelClass = Users::class;
    public $searchModelClass = UsersSearch::class;
    public function actionIndex()
    {
        $this->layout = 'login';
        // $model 
        return $this->render('index');
    }
    public function actionSignUp()
    {
        $this->layout = 'login';
        //$model
        return $this->render('sign_up');
    }
}
