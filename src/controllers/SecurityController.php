<?php

namespace ticmakers\adm\controllers;

use ticmakers\adm\components\AdmAssets;
use rmrevin\yii\fontawesome\AssetBundle;
use Yii;
use ticmakers\adm\components\actions\LoginAction;
use ticmakers\adm\components\actions\ChangePasswordAction;
use ticmakers\adm\components\actions\RegisterAction;
use ticmakers\adm\components\actions\RestorePasswordAction;
use ticmakers\adm\components\actions\SessionExpiredAction;
use ticmakers\adm\components\actions\ActiveAccountAction;
use ticmakers\core\base\Controller;

/**
 * SecurityController implementa las acciones para manejo de la sesión y control de acceso al sistema.
 *
 * @package ticmakers\adm
 * @subpackage controllers
 * @category Controllers
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class SecurityController extends Controller
{

    public $defaultAction = 'login';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'login' => [
                'class' => LoginAction::class,
                'isModal' => $this->isModal,
            ],
            'change-password' => [
                'class' => ChangePasswordAction::class,
                'isModal' => $this->isModal,
            ],
            'register' => [
                'class' => RegisterAction::class,
                'isModal' => $this->isModal,
            ],
            'restore-password' => [
                'class' => RestorePasswordAction::class,
                'isModal' => $this->isModal,
            ],
            'session-expired' => [
                'class' => SessionExpiredAction::class,
                'isModal' => $this->isModal,
            ],
            'active-account' => [
                'class' => ActiveAccountAction::class,
                'isModal' => $this->isModal,
            ],
            'change-password' => [
                'class' => ChangePasswordAction::class,
                'isModal' => $this->isModal,
            ]
        ];
    }
}
