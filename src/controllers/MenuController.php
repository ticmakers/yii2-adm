<?php

namespace ticmakers\adm\controllers;

use ticmakers\adm\models\base\MenuItems;
use ticmakers\adm\models\searchs\MenuItems as MenuItemsSearch;
use ticmakers\adm\models\base\Menus;
use ticmakers\adm\models\searchs\Menus as MenusSearch;
use ticmakers\core\base\Controller;

use ticmakers\sortablegridview\SortableAction;
use Yii;

/**
 * Controlador MenusController implementa las acciones para el CRUD de el modelo Menus.
 *
 * @package ticmakers/adm
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class MenuController extends Controller
{
    public $modelClass = Menus::class;
    public $searchModelClass = MenusSearch::class;
    public $searchModelItemsClass = MenuItemsSearch::class;

    /**
     * Undocumented variable
     *
     * @var array
     */
    public $actionsConfig = [
        'sortItem' => [
            'class' => SortableAction::class,
            'activeRecordClassName' => MenuItems::class,
            'orderColumn' => 'order',
        ]
    ];

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = Yii::createObject($this->modelClass);
        $searchModel = Yii::createObject($this->searchModelClass);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', 'It was created successfully'));

                return $this->redirect(['update', 'id' => $model->primaryKey, 'tab' => 2]);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'There was an error trying to create.'));
            }
        }

        return $this->render(
            'create',
            [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $searchModel = Yii::createObject($this->searchModelItemsClass);
        $searchModel->menu_id = $model->menu_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', 'It was updated successfully'));

                return $this->redirect(['update', 'id' => $model->primaryKey, 'tab' => 2]);
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar editar.'));
            }
        }

        return $this->render(
            'update',
            [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
