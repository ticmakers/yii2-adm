<?php

namespace ticmakers\adm\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ticmakers\adm\models\base\Menus as MenusModel;

/**
 * Esta clase representa las búsqueda para el modelo `ticmakers\adm\models\base\Menus`.
 *
 * @package ticmakers/adm
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 2.0.0
 */
class Menus extends MenusModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['menu_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'description', 'type', 'position', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenusModel::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'menu_id' => $this->menu_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
