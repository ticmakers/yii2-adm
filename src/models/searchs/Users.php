<?php

namespace ticmakers\adm\models\searchs;

use ticmakers\adm\models\base\Users as UsersModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Esta clase representa las búsqueda para el modelo `ticmakers\adm\models\base\Users`.
 *
 * @package ticmakers/adm
 * @subpackage models/searchs
 * @category Models
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Users extends UsersModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['username', 'email', 'password_hash', 'auth_key', 'password_reset_token', 'email_key', 'confirmed_email', 'blocked', 'accepted_terms', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsersModel::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email_key', $this->email_key])
            ->andFilterWhere(['like', 'confirmed_email', $this->confirmed_email])
            ->andFilterWhere(['like', 'blocked', $this->blocked])
            ->andFilterWhere(['like', 'accepted_terms', $this->accepted_terms])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
