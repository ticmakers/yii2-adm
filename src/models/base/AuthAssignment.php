<?php

namespace ticmakers\adm\models\base;

use ticmakers\adm\components\Model;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_assignment".
 * This entity stores all the items assigned to a user.
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $auth_assignment_id Assignment identifier
 * @property string $item_name Role identifier
 * @property integer $user_id Identifier user
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthAssignment extends Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['user_id', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name'], 'string', 'max' => 64],
            [[static::STATUS_COLUMN], 'default', 'value' => static::STATUS_ACTIVE],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'auth_assignment_id' => Yii::t($this->module->id, 'Código'),
            'item_name' => Yii::t($this->module->id, 'Item Name'),
            'user_id' => Yii::t($this->module->id, 'User'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'auth_assignment_id' => Yii::t($this->module->id, 'Assignment identifier'),
            'item_name' => Yii::t($this->module->id, 'Role identifier'),
            'user_id' => Yii::t($this->module->id, 'Identifier user'),
            'active' => Yii::t($this->module->id, 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t($this->module->id, 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t($this->module->id, 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|AuthAssignment[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'auth_assignment_id', self::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Asignar los roles al usuario, registrarlos en la bd y retornar roles asignados y disponibles
     *
     * @param int $userId | identificador del usuario
     * @param array[] $items | listado de roles para asignar
     * @return array[] $roleList | listado de roles asignados y disponibles
     */
    public function assignItems($userId, $items = [])
    {
        $response = [
            'status' => true,
        ];
        if (!empty($items)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($items as $role) {
                    $oldModel = AuthAssignment::findOne([
                        'user_id' => $userId,
                        'item_name' => $role,
                    ]);
                    if (empty($oldModel)) {
                        $model = new AuthAssignment();
                        $model->user_id = $userId;
                        $model->item_name = $role;
                        $model->save();
                    } else {
                        $oldModel->active = 'Y';
                        $oldModel->save(true, ['active']);
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'items' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['items']['available'] = AuthItem::getAvailableItems($userId);
                $response['items']['assigned'] = AuthItem::getAssignedItems($userId);
            }
        }
        return $response;
    }

    /**
     * Remover los roles del usuario, registralo en la bd y retornar listado de roles
     * asignados y removidos
     *
     * @param int $userId | identificador del usuario
     * @param array[] $items | listado de roles para asignar
     * @return array[] $roleList | listado de roles asignados y disponibles
     * @return
     */
    public function revokeItems($userId, $items = [])
    {
        $response = [
            'status' => true,
        ];
        if (!empty($items)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($items as $role) {
                    $model = AuthAssignment::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'user_id' => $userId,
                            'item_name' => $role,
                        ]
                    );
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'items' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['items']['available'] = AuthItem::getAvailableItems($userId);
                $response['items']['assigned'] = AuthItem::getAssignedItems($userId);
            }
        }
        return $response;
    }
}
