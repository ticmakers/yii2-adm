<?php

namespace ticmakers\adm\models\base;

use ticmakers\adm\components\Model;
use Yii;

/**
 * Éste es el modelo para la tabla "social_networks".
 *
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $social_network_id Social network identifier
 * @property integer $user_id User identifier
 * @property string $social_network_user User identifier at social network
 * @property string $name Abbreviation of the name of the social network FAB -> Facebook; GOP -> Google Plus.
 * @property string $response Information provided by the social network
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $update_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property Users $user Datos relacionados con modelo "Users"
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class SocialNetworks extends Model
{
    const SCENARIO_LOGIN = 'login';

    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'social_networks';
    }

    /**
     * Configuración de los scenarios
     *
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_LOGIN => [ 'social_network_user', 'name', 'response' ]
        ]);
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'social_network_user', 'name'], 'required'],
            [['user_id', 'updated_by'], 'integer'],
            [['response'], 'string'],
            [['created_at', 'update_at'], 'safe'],
            [['social_network_user'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 3],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'social_network_id' => Yii::t('app', 'Código'),
            'user_id' => Yii::t('app', 'User'),
            'social_network_user' => Yii::t('app', 'Social Network User'),
            'name' => Yii::t('app', 'Name'),
            'response' => Yii::t('app', 'Response'),
            'active' => Yii::t('app', 'Active'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'social_network_id' => Yii::t('app', 'Social network identifier'),
            'user_id' => Yii::t('app', 'User identifier'),
            'social_network_user' => Yii::t('app', 'User identifier at social network '),
            'name' => Yii::t('app', 'Abbreviation of the name of the social network FAB -> Facebook; GOP -> Google Plus.'),
            'response' => Yii::t('app', 'Information provided by the social network'),
            'active' => Yii::t('app', 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t('app', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('app', 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t('app', 'It\'s the identifier of the user who updated the record.'),
            'update_at' => Yii::t('app', 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "User".
     *
     * @return \app\models\base\User
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|SocialNetworks[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'social_network_id', self::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Realiza el login por redes sociales.
     * 
     * @return boolean
     */
    public function login()
    {
        $this->scenario = self::SCENARIO_LOGIN;
        if ($this->validate()) {
            dd('Aquí la validación de login por redes sociales.');
        }
        return true;
    }
}