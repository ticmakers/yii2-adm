<?php

namespace ticmakers\adm\models\base;use Yii;
use ticmakers\adm\components\Model;

/**
 * Éste es el modelo para la tabla "auth_item_child".
 * Stores the hierarchy of authorization items.
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $auth_item_child_id Auth item child identifier
 * @property string $parent Item parent identifier
 * @property string $child Item child identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by  It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthItemChild extends Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'auth_item_child';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['parent', 'child'], 'string', 'max' => 64],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'auth_item_child_id' => Yii::t($this->module->id, 'Código'),
            'parent' => Yii::t($this->module->id, 'Parent'),
            'child' => Yii::t($this->module->id, 'Child'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'auth_item_child_id' => Yii::t($this->module->id, 'Auth item child identifier'),
            'parent' => Yii::t($this->module->id, 'Item parent identifier'),
            'child' => Yii::t($this->module->id, 'Item child identifier'),
            'active' => Yii::t($this->module->id, 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t($this->module->id, ' It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t($this->module->id, 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t($this->module->id, 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|AuthItemChild[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'auth_item_child_id', self::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Metodo para asignar las rutas seleccionadas a un permiso
     *
     * @return array []
     */
    public function assignRoutesPermission($items = [], $parent)
    {
        $response = [
            'status' => true,
            'routes' => [],
        ];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($items as $child) {
                $oldModel = AuthItemChild::findOne(
                    [
                        'parent' => $parent,
                        'child' => $child,
                    ]
                );
                if (empty($oldModel)) {
                    $modelPermission = new AuthItemChild();
                    $modelPermission->parent = $parent;
                    $modelPermission->child = $child;
                    $modelPermission->save(false);
                } else {
                    $oldModel->active = 'Y';
                    $oldModel->save(false);
                }

            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response = [
                'status' => false,
                'Routes' => [],
            ];
            throw $e;
        }
        if (isset($response['status']) && $response['status']) {
            $response['routes']['available'] = AuthItem::getAvailableRoutesPermission($parent);
            $response['routes']['assigned'] = AuthItem::getAssignedRoutesPermission($parent);
        }
        return $response;
    }

    /**
     * Remover las rutas seleccionadas de un permiso
     *
     * @param array[] $routes | listado de rutas para asignar
     */
    public function revokeRoutesPermission($routes = [], $parent)
    {
        $response = [
            'status' => true,
        ];
        if (!empty($routes)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($routes as $child) {
                    AuthItemChild::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'parent' => $parent,
                            'child' => $child,
                        ]);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'routes' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['routes']['available'] = AuthItem::getAvailableRoutesPermission($parent);
                $response['routes']['assigned'] = AuthItem::getAssignedRoutesPermission($parent);
            }
        }
        return $response;
    }

    /**
     * Metodo para asignar los items seleccionados a un rol
     *
     * @return array []
     */
    public function assignItemsToRoles($items = [], $parent)
    {
        $response = [
            'status' => true,
        ];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($items as $child) {
                $oldModel = AuthItemChild::findOne(
                    [
                        'parent' => $parent,
                        'child' => $child,
                    ]
                );
                if (empty($oldModel)) {
                    $modelPermission = new AuthItemChild();
                    $modelPermission->parent = $parent;
                    $modelPermission->child = $child;
                    $modelPermission->save(false);
                } else {
                    $oldModel->active = 'Y';
                    $oldModel->save(false);
                }

            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response = [
                'status' => false,
                'roleItems' => [],
            ];
            throw $e;
        }
        if (isset($response['status']) && $response['status']) {
            $response['roleItems']['available'] = AuthItem::getAvailableRoleItems($parent);
            $response['roleItems']['assigned'] = AuthItem::getAssignedRoleItems($parent);
        }
        return $response;
    }

    /**
     * Remover las items(roles, permisos, rutas) seleccionados de un rol
     *
     * @param array[] $routes | listado de items asignados
     */
    public function revokeItemsToRoles($items = [], $parent)
    {
        $response = [
            'status' => true,
        ];
        if (!empty($items)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($items as $child) {
                    AuthItemChild::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'parent' => $parent,
                            'child' => $child,
                        ]);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'roleItems' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['roleItems']['available'] = AuthItem::getAvailableRoleItems($parent);
                $response['roleItems']['assigned'] = AuthItem::getAssignedRoleItems($parent);
            }
        }
        return $response;
    }
}
