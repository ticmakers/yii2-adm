<?php

namespace ticmakers\adm\models\base;

use ticmakers\adm\components\Model;
use Yii;

/**
 * Éste es el modelo para la tabla "menu_items".
 * This entity stores all the menu items
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $menu_item_id Identifier menu item
 * @property integer $menu_id
 * @property string $name Menu name
 * @property integer $parent_menu_id Parent menu identifier
 * @property string $internal Define if item is internal or external. Format Y-> Yes  N-> No
 * @property string $route_id Route identifier
 * @property string $link Link to the one that redirects the menu.
 * @property string $icon Icon associated to the menu item
 * @property integer $order Order in which the menu item is displayed
 * @property string $target Define where the content of the menu item will be displayed, format: SELF = En la misma pestaña BLANK = En una pestaña nueva
 * @property string $params Item parameters
 * @property string $description Description of the menu item
 * @property string $active Define if it's active or not. Format : Y-> Yes N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $route Datos relacionados con modelo "AuthItem"
 * @property MenuItems $parentMenu Datos relacionados con modelo "MenuItems"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property Menus $menu Datos relacionados con modelo "Menus"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class MenuItems extends Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'menu_items';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['menu_id', 'name', 'internal', 'order', 'target'], 'required'],
            [['menu_id', 'parent_menu_id', 'order', 'updated_by'], 'integer'],
            [['link', 'params', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'route_id', 'icon'], 'string', 'max' => 64],
            [['internal'], 'string', 'max' => 1],
            [['target'], 'string', 'max' => 10],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['route_id' => 'name']],
            [['parent_menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItems::className(), 'targetAttribute' => ['parent_menu_id' => 'menu_item_id']],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menus::className(), 'targetAttribute' => ['menu_id' => 'menu_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'menu_item_id' => Yii::t($this->module->id, 'Código'),
            'menu_id' => Yii::t($this->module->id, 'Menu'),
            'name' => Yii::t($this->module->id, 'Name'),
            'parent_menu_id' => Yii::t($this->module->id, 'Parent Menu'),
            'internal' => Yii::t($this->module->id, 'Internal'),
            'route_id' => Yii::t($this->module->id, 'Route'),
            'link' => Yii::t($this->module->id, 'Link'),
            'icon' => Yii::t($this->module->id, 'Icon'),
            'order' => Yii::t($this->module->id, 'Order'),
            'target' => Yii::t($this->module->id, 'Target'),
            'params' => Yii::t($this->module->id, 'Params'),
            'description' => Yii::t($this->module->id, 'Description'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'menu_item_id' => Yii::t($this->module->id, 'Identifier menu item'),
            'menu_id' => Yii::t($this->module->id, ''),
            'name' => Yii::t($this->module->id, 'Menu name'),
            'parent_menu_id' => Yii::t($this->module->id, 'Parent menu identifier'),
            'internal' => Yii::t($this->module->id, 'Define if item is internal or external. Format Y-> Yes  N-> No'),
            'route_id' => Yii::t($this->module->id, 'Route identifier'),
            'link' => Yii::t($this->module->id, 'Link to the one that redirects the menu.'),
            'icon' => Yii::t($this->module->id, 'Icon associated to the menu item'),
            'order' => Yii::t($this->module->id, 'Order in which the menu item is displayed'),
            'target' => Yii::t($this->module->id, 'Define where the content of the menu item will be displayed, format: SELF = En la misma pestaña BLANK = En una pestaña nueva'),
            'params' => Yii::t($this->module->id, 'Item parameters'),
            'description' => Yii::t($this->module->id, 'Description of the menu item'),
            'active' => Yii::t($this->module->id, 'Define if it\'s active or not. Format : Y-> Yes N -> No'),
            'created_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t($this->module->id, 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t($this->module->id, 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Route".
     *
     * @return \ticmakers\adm\models\base\Route
     */
    public function getRoute()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'route_id']);
    }

    /**
     * Definición de la relación con el modelo "ParentMenu".
     *
     * @return \ticmakers\adm\models\base\ParentMenu
     */
    public function getParentMenu()
    {
        return $this->hasOne(MenuItems::className(), ['menu_item_id' => 'parent_menu_id']);
    }

    /**
     * Definición de la relación con el modelo "MenuItems".
     *
     * @return \ticmakers\adm\models\base\MenuItems
     */
    public function getMenuItems()
    {
        $query = $this->hasMany(MenuItems::className(), ['parent_menu_id' => 'menu_item_id']);
        $query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
        return $query;
    }

    /**
     * Definición de la relación con el modelo "Menu".
     *
     * @return \ticmakers\adm\models\base\Menu
     */
    public function getMenu()
    {
        return $this->hasOne(Menus::className(), ['menu_id' => 'menu_id']);
    }

    /**
     * Definición de la relación con el modelo "RoleMenuItems".
     *
     * @return \ticmakers\adm\models\base\RoleMenuItems
     */
    public function getRoleMenuItems()
    {
        $query = $this->hasMany(RoleMenuItems::className(), ['menu_item_id' => 'menu_item_id']);
        $query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
        return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|MenuItems[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'menu_item_id', self::getNameFromRelations());
        }
        return $query;
    }
    /**
     * Método encargado de entregar el nombre de la columna para relaciones
     *
     * @return string
     */
    public static function getNameFromRelations()
    {
        return 'name';
    }
}
