<?php

namespace ticmakers\adm\models\base;

use ticmakers\adm\components\Model;
use ticmakers\notifications\models\base\Notifications;
use ticmakers\notifications\models\searchs\NotificationRecipients;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * Éste es el modelo para la tabla "users".
 * This entity stores all the users.
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $user_id User identifier
 * @property string $username Username with which the user logs in.
 * @property string $email User email
 * @property string $password_hash Password encrypted.
 * @property string $auth_key Random code that identifies the access key for resources by the api rest
 * @property string $password_reset_token Random code to identify when to reset the password
 * @property string $email_key Key to validate that the email is valid
 * @property string $confirmed_email Establishes if the email is confirmed. Format  Y -> Yes  N -> No
 * @property string $blocked Establishes if the user is blocked  Y -> Yes  N -> No
 * @property string $accepted_terms It determines if the user accepts or not the terms and conditions.  Format: Y->Yes or N->No
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by  It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Users extends Model implements IdentityInterface
{
    public $password;
    public $confirmPassword;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_EXPIRED = 'expired';
    const SCENARIO_CHANGE = 'change';
    const SCENARIO_ACTIVE = 'active';

    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    const STATUS_COLUMN = 'active';
    const BLOCKED_COLUMN = 'blocked';

    /**
     * Configuración inicial
     */
    public function init()
    {
        parent::init();
        $this->{static::STATUS_COLUMN} = static::STATUS_ACTIVE;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['active', 'default', 'value' => static::STATUS_ACTIVE],
            [['blocked', 'confirmed_email'], 'default', 'value' => static::STATUS_INACTIVE],
            [['username', 'email', 'confirmed_email', 'blocked'], 'required', 'on' => [static::SCENARIO_CREATE, static::SCENARIO_UPDATE]],
            [['email', 'password'], 'required', 'on' => [static::SCENARIO_EXPIRED]],
            [['email', 'password', 'confirmPassword'], 'required', 'on' => [static::SCENARIO_CHANGE]],
            [['password', 'confirmPassword'], 'required', 'on' => static::SCENARIO_CREATE],
            [['password_hash', 'password_reset_token', 'email_key'], 'string'],
            [
                ['username', 'email'],
                'unique',
                'on' => [static::SCENARIO_CREATE, static::SCENARIO_UPDATE],
                'message' => Yii::t($this->module->id, "No es posible crear el usuario debido a que el {attribute}, ya se encuentra registrado."),
                'filter' => function ($query) {
                    $query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE]);
                }
            ],
            [
                'confirmPassword', 'compare', 'compareAttribute' => 'password',
                'message' => Yii::t($this->module->id, 'Las contraseñas no coinciden. Por favor verifique')
            ],
            [['password', 'confirmPassword'], 'string'],
            [['password', 'confirmPassword'], 'string', 'min' => 8, 'max' => 16],
            [['email'], 'match', 'pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'],
            [['created_at', 'updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['username', 'email'], 'string', 'max' => 255],
            ['email', 'required', 'on' => self::SCENARIO_ACTIVE],
            [['auth_key'], 'string', 'max' => 32],
            [['confirmed_email', 'blocked', 'accepted_terms'], 'string', 'max' => 1],
        ];
        if ($this->module->useTermsAndConditions) {
            $rules[] = [
                'accepted_terms', 'required', 'requiredValue' => 1,
                'message' => Yii::t($this->module->id, 'You must be accept the terms and conditions'), 'whenClient' => 'function(attribute, value){
                $(attribute.container).parent().find(".invalid-feedback").css("display", value == 0 ? "block": "none")
                return true; 
            }', 'on' => [self::SCENARIO_CREATE]
            ];
        };
        return $rules;
    }

    /**
     * Permite obtener el identificador del usuario activo
     *
     * @return void
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t($this->module->id, 'Código'),
            'username' => Yii::t($this->module->id, 'Username'),
            'email' => Yii::t($this->module->id, 'Email'),
            'password_hash' => Yii::t($this->module->id, 'Password Hash'),
            'password' => Yii::t($this->module->id, 'Password'),
            'confirmPassword' => Yii::t($this->module->id, 'Confirm password'),
            'auth_key' => Yii::t($this->module->id, 'Auth Key'),
            'password_reset_token' => Yii::t($this->module->id, 'Password Reset Token'),
            'email_key' => Yii::t($this->module->id, 'Email Key'),
            'confirmed_email' => Yii::t($this->module->id, 'Confirmed Email'),
            'blocked' => Yii::t($this->module->id, 'Blocked'),
            'accepted_terms' => Yii::t($this->module->id, 'Accepted Terms'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'user_id' => Yii::t($this->module->id, 'User identifier'),
            'username' => Yii::t($this->module->id, 'Username with which the user logs in.'),
            'email' => Yii::t($this->module->id, 'User email'),
            'password_hash' => Yii::t($this->module->id, 'Password encrypted.'),
            'password' => Yii::t($this->module->id, 'User password.'),
            'confirmPassword' => Yii::t($this->module->id, 'Confirm user password.'),
            'auth_key' => Yii::t($this->module->id, 'Random code that identifies the access key for resources by the api rest'),
            'password_reset_token' => Yii::t($this->module->id, 'Random code to identify when to reset the password   '),
            'email_key' => Yii::t($this->module->id, 'Key to validate that the email is valid'),
            'confirmed_email' => Yii::t($this->module->id, 'Establishes if the email is confirmed. Format  Y -> Yes  N -> No'),
            'blocked' => Yii::t($this->module->id, 'Establishes if the user is blocked  Y -> Yes  N -> No'),
            'accepted_terms' => Yii::t($this->module->id, 'It determines if the user accepts or not the terms and conditions.  Format: Y->Yes or N->No'),
            'active' => Yii::t($this->module->id, 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t($this->module->id, 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t($this->module->id, 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];

        return $helps[$attribute] ?? (is_null($attribute) ? $helps : '');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where([
                'user_id' => $id,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su token de acceso
     *
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->where([
                'auth_key' => $token,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su nombre de usuario
     *
     * @param string $username
     * @return Users|null
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->where([
                'username' => $username,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su llave de email
     *
     * @param string $emailKey
     * @return Users|null
     */
    public static function findByEmailKey($emailKey)
    {
        return static::find()
            ->where([
                'email_key' => $emailKey,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su correo electronico
     * 
     * @return Users|null
     */
    public static function findByName($email)
    {
        return static::find()
            ->where([
                'email' => $email,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su token para cambiar contraseña
     * 
     * @return Users|null
     */
    public static function findByPasswordResetToken($token)
    {
        return static::find()
            ->where([
                'password_reset_token' => $token,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtiene el usuario por su correo electrónico o nombre de usuario
     * 
     * @return Users|null
     */
    public static function findByUsernameOrEmail($emailUsername)
    {
        return static::find()
            ->where([
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->andWhere([
                'or',
                ['username' => $emailUsername],
                ['email' => $emailUsername]
            ])->one();
    }

    /**
     * Generar auth_key, password hash y asignarlo al modelo antes de guardarlo
     *
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!empty($this->password)) {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        }
        if (empty($this->email_key)) {
            $this->email_key = Yii::$app->security->generateRandomString();
        }
        $this->accepted_terms = $this->accepted_terms == 1 ? static::STATUS_ACTIVE : static::STATUS_INACTIVE;

        return parent::beforeSave($insert);
    }

    /**
     * Genera el password hash basado en la contraseña y lo asigna al modelo
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Users[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }
        if (!isset($attributes[static::BLOCKED_COLUMN])) {
            $attributes[static::BLOCKED_COLUMN] = static::STATUS_INACTIVE;
        }
        $query = static::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'user_id', static::getNameFromRelations());
        }
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Enviar el correo de confirmacion de la cuenta al email de usuario
     *
     * @return void
     */
    public function sendEmailConfirmation($email)
    {
        $user = static::findByName($email);
        $url = 'security/active-account';
        $template = '@ticmakers/adm/mail/email_confirmation';
        if (Yii::$app->hasModule('notifications')) {

            $notificationModule = Yii::$app->getModule('notifications');
            $channel = $notificationModule->getChannel('E' /*EmailChannel::TYPE_CHANNEL*/);
            $channel->template = $template;

            $notificationModel = new Notifications([
                'type' => $channel->type,
                'title' => 'Confirmación de la cuenta',
                'message' => 'confirmación',
                'additional_information' => Json::encode([
                    'applicationName' => Yii::$app->name,
                    'username' => $user->username,
                    'logo' => Yii::getAlias('@app') . '/web/imagenes/logo_plantilla_correo.png',
                    'urlConfirmation' => Url::to([$url, 'token' => $user->email_key], true),
                ]),
                'generated_from' => $this->module->id,
            ]);
            $notificationModel->save();
            $recipentsModel = new NotificationRecipients([
                'notification_id' => $notificationModel->notification_id,
                'recipient' => $email,
                'user_id' => $user->user_id,
                'status' => 'P',
            ]);
            $recipentsModel->save();

            return $channel->send($notificationModel);
        } else {
            throw new Exception("Error Processing Request", 1);
        }
    }

    /**
     * Enviar el correo de restauración de contraseña para un usario registrado
     *
     * @return void
     */
    public function sendRestorePasswordEmail()
    {
        $url = 'security/change-password';
        $typeChannel = 'E';
        $template = '@ticmakers/adm/mail/reset_password';
        if (Yii::$app->hasModule('notifications')) {
            $this->validatePasswordResetToken();
            $channel = self::getChannelEmail($typeChannel, $template);
            $notificationModel = new Notifications([
                'type' => $channel->type,
                'title' => 'Cambio de contraseña',
                'message' => 'Cambio contraseña',
                'additional_information' => Json::encode([
                    'applicationName' => Yii::$app->name,
                    'username' => $this->username,
                    'logo' => Yii::getAlias('@app') . '/web/imagenes/logo_plantilla_correo.png',
                    'urlConfirmation' => Url::to([$url, 'token' => $this->password_reset_token], true),
                ]),
                'generated_from' => $this->module->id,
            ]);
            $notificationModel->save();
            $recipentsModel = new NotificationRecipients([
                'notification_id' => $notificationModel->notification_id,
                'recipient' => $this->email,
                'user_id' => $this->user_id,
                'status' => 'P',
            ]);
            $recipentsModel->save();
            return $channel->send($notificationModel);
        } else {
            throw new Exception("Error Processing Request", 1);
        }
    }

    /**
     * Permite obtener el canal de envio de notificaicon para las notificaciones tipo email
     * 
     * @return Object $channel | canal de la notificacion tipo email
     */
    public function getChannelEmail($type, $template)
    {
        $notificationModule = Yii::$app->getModule('notifications');
        $channel = $notificationModule->getChannel($type);
        $channel->template = $template;
        return $channel;
    }
    /**
     * Valida el token para cambio de contraseña para generar uno en caso de que no exista
     * 
     * @return boolean
     */
    public function validatePasswordResetToken()
    {

        if (empty($this->password_reset_token)) {
            $this->generatePasswordResetToken();
        }
        $this->save();
    }

    /**
     * Validar tiempo de expiración para el token de cambio de contraseña
     * 
     * @return boolean
     */
    public function validateExpireTimeToken()
    {
        $expireTime = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $this->password_reset_token);
        $timestamp = (int) end($parts);
        return ($timestamp + $expireTime) >= time();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function formColumns()
    {
        $formColumns = parent::formColumns();
        unset($formColumns['password_hash'],
        $formColumns['auth_key'],
        $formColumns['password_reset_token'],
        $formColumns['email_key']);

        $formColumns['password'] = [
            "attribute" => "password",
            "containerOptions" => [
                'class' => 'col-12 col-md-6'
            ],
            "onlyInSearch" => false,
            "render" => [
                "C",
                "U"
            ],
            'type' => 'passwordInput',
            "fieldOptions" => [
                "tabindex" => 98
            ]
        ];

        $formColumns['confirmPassword'] = [
            "attribute" => "confirmPassword",
            "containerOptions" => [
                'class' => 'col-12 col-md-6'
            ],
            "onlyInSearch" => false,
            "render" => [
                "C",
                "U"
            ],
            'type' => 'passwordInput',
            "fieldOptions" => [
                "tabindex" => 99
            ]
        ];

        return $formColumns;
    }
}
