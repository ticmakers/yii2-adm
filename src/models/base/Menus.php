<?php

namespace ticmakers\adm\models\base;

use ticmakers\adm\components\Model;
use Yii;

/**
 * Éste es el modelo para la tabla "menus".
 * This entity stores all the menus.
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $menu_id Menu identifier
 * @property string $name Menu name
 * @property string $description Menu's description
 * @property string $type Define the menu type:  B-> Backend F-> Frontend M -> Movil
 * @property string $position Define the location of the menu
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Menus extends Model
{
    const BACKEND = 'B';
    const FRONTEND = 'F';
    const MOVIL = 'M';
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'position', 'type'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['type'], 'string', 'max' => 1],
            [['position'], 'string', 'max' => 64],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => Yii::t($this->module->id, 'Code'),
            'name' => Yii::t($this->module->id, 'Name'),
            'description' => Yii::t($this->module->id, 'Description'),
            'type' => Yii::t($this->module->id, 'Type'),
            'position' => Yii::t($this->module->id, 'Position'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'menu_id' => Yii::t($this->module->id, 'Menu identifier'),
            'name' => Yii::t($this->module->id, 'Menu name'),
            'description' => Yii::t($this->module->id, 'Menu\'s description'),
            'type' => Yii::t($this->module->id, 'Define the menu type:  B-> Backend F-> Frontend M -> Movil'),
            'position' => Yii::t($this->module->id, 'Define the location of the menu'),
            'active' => Yii::t($this->module->id, 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t($this->module->id, 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t($this->module->id, 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "MenuItems".
     *
     * @return \ticmakers\adm\models\base\MenuItems
     */
    public function getMenuItems()
    {
        $query = $this->hasMany(MenuItems::className(), ['menu_id' => 'menu_id']);
        $query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
        return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Menus[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'menu_id', self::getNameFromRelations());
        }
        return $query;
    }

    public static function getTypes($index = null)
    {
        $res = [
            self::BACKEND => Yii::t('app', 'Backend'),
            self::FRONTEND => Yii::t('app', 'Frontend'),
            self::MOVIL => Yii::t('app', 'Movil')
        ];
        return $index == null ? $res : $res[$index];
    }
    /**
     * Permite obtener los items del menú de acuerdo al rol de usuario
     * 
     * @return array | $resultItems: listado de items del menú
     */
    public function getItems()
    {

        $sql = "SELECT mih.name, (CASE WHEN mih.parent_menu_id IS NULL
                THEN concat(mih.order::text, '0','0')
                    ELSE concat(mip.order::text, '1',mih.order::text)
                END)::numeric as \"order\",
                mih.menu_item_id,
                mih.parent_menu_id,
                mih.icon,
                mih.route_id,
                mih.target,
                mih.link,
                mih.internal, 
                mih.params
            FROM menu_items mih
            LEFT JOIN menu_items mip ON mih.parent_menu_id = mip.menu_item_id
            WHERE mih.menu_id =:menuId and mih.active =:active
            ORDER BY 2";

        $items = Yii::$app->db->createCommand($sql, [
            ':menuId' => $this->menu_id,
            ':active' => static::STATUS_ACTIVE
        ])->queryAll();
        $resultItems = [];
        $menuOfRol = array_map(function ($data) {
            return $data->menu_item_id;
        }, static::getMenusForRol());
        foreach ($items as $item) {
            $finalParams = [];
            if ($item['params']) {
                $params =  explode(',', $item['params']);
                foreach ($params as $value) {
                    $explode = explode('=', $value);
                    $finalParams[$explode[0]] = $explode[1];
                }
            }

            if (is_null($item['parent_menu_id']) && in_array($item['menu_item_id'], $menuOfRol)) {
                $resultItems[$item['menu_item_id']] = [
                    'label' => $item['name'],
                    'url' => array_merge([$item['route_id']], $finalParams),
                    'icon' => $item['icon'],
                    'link' => $item['link'],
                    'internal' => $item['internal'],
                    'target' => $item['target']
                ];
            } elseif (in_array($item['parent_menu_id'], $menuOfRol)) {
                if (isset($resultItems[$item['parent_menu_id']]['items'])) {
                    $resultItems[$item['parent_menu_id']]['items'][] = [
                        'label' => $item['name'],
                        'url' => array_merge([$item['route_id']], $finalParams),
                    ];
                } else {
                    $resultItems[$item['parent_menu_id']]['items'] = [
                        [
                            'label' => $item['name'],
                            'url' => array_merge([$item['route_id']], $finalParams),
                        ],
                    ];
                }
                $resultItems[$item['parent_menu_id']]['url'] = 'javascript:;';
            }
        }
        return $resultItems;
    }

    /**
     * Retorna los items del menú asignados al rol
     * 
     * @return array
     */
    public static function getMenusForRol()
    {
        $result = [];
        $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        if (!empty($roles)) {
            $result = MenuItems::findBySql("
            SELECT mei.*
            FROM menu_items mei
            INNER JOIN role_menu_items rmi ON mei.menu_item_id = rmi.menu_item_id AND rmi.role_id IN ('" . implode("','", array_keys($roles)) . "') AND rmi.active = :active
            WHERE rmi.active = :active
            ", [
                ':active' => 'Y'
            ])->all();
        }
        return $result;
    }
}
