<?php

namespace ticmakers\adm\models\base;

use Yii;
use ticmakers\adm\components\Model;

/**
 * Éste es el modelo para la tabla "role_menu_items".
 * This entity stores all the active menus for a role.
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $role_menu_item_id Identifier of a menu item associated with a role.
 * @property string $role_id Role identifier
 * @property integer $menu_item_id Menu item identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $role Datos relacionados con modelo "AuthItem"
 * @property MenuItems $menuItem Datos relacionados con modelo "MenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class RoleMenuItems extends Model
{

    public $roles;

    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'role_menu_items';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['roles', 'menu_item_id'], 'required'],
            [['menu_item_id', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['role_id'], 'string', 'max' => 64],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['role_id' => 'name']],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItems::className(), 'targetAttribute' => ['menu_item_id' => 'menu_item_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'role_menu_item_id' => Yii::t('app', 'Código'),
            'role_id' => Yii::t('app', 'Role'),
            'menu_item_id' => Yii::t('app', 'Menu Item'),
            'active' => Yii::t('app', 'Active'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'role_menu_item_id' => Yii::t('app', 'Identifier of a menu item associated with a role.'),
            'role_id' => Yii::t('app', 'Role identifier'),
            'menu_item_id' => Yii::t('app', 'Menu item identifier'),
            'active' => Yii::t('app', 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t('app', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('app', 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t('app', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('app', 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Role".
     *
     * @return \ticmakers\adm\models\base\Role
     */
    public function getRole()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'role_id']);
    }

    /**
     * Definición de la relación con el modelo "MenuItem".
     *
     * @return \ticmakers\adm\models\base\MenuItem
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItems::className(), ['menu_item_id' => 'menu_item_id']);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|RoleMenuItems[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'role_menu_item_id', self::getNameFromRelations());
        }
        return $query;
    }
}
