<?php

namespace ticmakers\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_rule".
 * This entity stores all the rules
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property string $name Rule identifier
 * @property string $data Additional value of the rule identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthRule extends \ticmakers\core\base\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'auth_rule';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['data'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t($this->module->id, 'Código'),
            'data' => Yii::t($this->module->id, 'Data'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'name' => Yii::t($this->module->id, 'Rule identifier'),
            'data' => Yii::t($this->module->id, 'Additional value of the rule identifier'),
            'active' => Yii::t($this->module->id, 'Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t($this->module->id, 'Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => Yii::t($this->module->id, 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t($this->module->id, 'Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|AuthRule[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'name', self::getNameFromRelations());
        }
        return $query;
    }
}
