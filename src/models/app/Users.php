<?php

namespace ticmakers\adm\models\app;

use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\app\NotificationRecipients;
use ticmakers\notifications\models\app\Notifications;
use yii\helpers\Json;
use yii\helpers\Url;
use Yii;

/**
 * Éste es el modelo para la tabla "users".
 * 
 *
 * @package ticmakers\adm\models\app 
 *
 * @property integer $user_id User identifier
 * @property string $username Username with which the user logs in.
 * @property string $email User email
 * @property string $password_hash Password encrypted.
 * @property string $auth_key Random code that identifies the access key for resources by the api rest
 * @property string $password_reset_token Random code to identify when to reset the password   
 * @property string $email_key Key to validate that the email is valid
 * @property string $confirmed_email Establishes if the email is confirmed. Format  Y -> Yes  N -> No
 * @property string $blocked Establishes if the user is blocked  Y -> Yes  N -> No
 * @property string $accepted_terms It determines if the user accepts or not the terms and conditions.  Format: Y->Yes or N->No
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by  It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property Actors[] $actors Datos relacionados con modelo "Actors"
 * @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
 * @property SocialNetworks[] $socialNetworks Datos relacionados con modelo "SocialNetworks"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 */
class Users extends \ticmakers\adm\models\base\Users implements \yii\web\IdentityInterface
{

    public $password;
    public $confirmPassword;
    const SCENARIO_CREATE = 'default';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_EXPIRED = 'expired';
    const SCENARIO_CHANGE = 'change';
    const SCENARIO_ACTIVE = 'active';

    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    const STATUS_COLUMN = 'active';
    const BLOCKED_COLUMN = 'blocked';

    public function init()
    {
        parent::init();
        $this->blocked = 'N';
    }

    /**
     * @inheritDoc
     */
    // public function rules()
    // {
    //     $rules = [];
    //     return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    // }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['active', 'default', 'value' => static::STATUS_ACTIVE],
            [['blocked', 'confirmed_email'], 'default', 'value' => static::STATUS_INACTIVE],
            [['username', 'email', 'confirmed_email', 'blocked'], 'required', 'on' => [static::SCENARIO_CREATE, static::SCENARIO_UPDATE]],
            [['email', 'password'], 'required', 'on' => [static::SCENARIO_EXPIRED]],
            [['email', 'password', 'confirmPassword'], 'required', 'on' => [static::SCENARIO_CHANGE]],
            [['password', 'confirmPassword'], 'required', 'on' => static::SCENARIO_CREATE],
            [['password_hash', 'password_reset_token', 'email_key'], 'string'],
            [
                ['username', 'email'],
                'unique',
                'on' => [static::SCENARIO_CREATE, static::SCENARIO_UPDATE],
                'message' => Yii::t($this->module->id, "No es posible crear el usuario debido a que el {attribute}, ya se encuentra registrado."),
                'filter' => function ($query) {
                    $query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE]);
                }
            ],
            [
                'confirmPassword', 'compare', 'compareAttribute' => 'password',
                'message' => Yii::t($this->module->id, 'Las contraseñas no coinciden. Por favor verifique')
            ],
            [['password', 'confirmPassword'], 'string'],
            [['password', 'confirmPassword'], 'string', 'min' => 8, 'max' => 16],
            [['email'], 'match', 'pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'],
            [['created_at', 'updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['username', 'email'], 'string', 'max' => 255],
            ['email', 'required', 'on' => self::SCENARIO_ACTIVE],
            [['auth_key'], 'string', 'max' => 32],
            [['confirmed_email', 'blocked', 'accepted_terms'], 'string', 'max' => 1],
        ];
        if ($this->module->useTermsAndConditions) {
            $rules[] = [
                'accepted_terms', 'required', 'requiredValue' => 1,
                'message' => Yii::t($this->module->id, 'You must be accept the terms and conditions'), 'whenClient' => 'function(attribute, value){
                $(attribute.container).parent().find(".invalid-feedback").css("display", value == 0 ? "block": "none")
                return true; 
            }', 'on' => [self::SCENARIO_CREATE]
            ];
        };
        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [
            'password' => Yii::t('adm', 'Password'),
            'confirmPassword' => Yii::t('adm', 'Confirm password'),
        ];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [
            'password' => Yii::t('adm', ''),
            'confirmPassword' => Yii::t('adm', '')
        ];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $parentFormColumns = parent::formColumns();
        $parentFormColumns['password_hash']['render'] = [];
        $parentFormColumns['auth_key']['render'] = [];
        $parentFormColumns['password_reset_token']['render'] = [];
        $parentFormColumns['email_key']['render'] = [];
        $formColumns = [
            'confirmed_email' => [
                'widget' => [
                    'class' => Select2::class,
                    'data' => Yii::$app->strings::getCondition(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty()
                    ]
                ]
            ],
            'blocked' => [
                'widget' => [
                    'class' => Select2::class,
                    'data' => Yii::$app->strings::getCondition(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty()
                    ]
                ]
            ],
            'accepted_terms' => [
                'widget' => [
                    'class' => Select2::class,
                    'data' => Yii::$app->strings::getCondition(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty()
                    ]
                ]
            ],
            'password' => [
                'attribute' => 'password',
                'render' => ['C', 'U'],
                'type' => 'passwordInput'
            ],
            'confirmPassword' => [
                'attribute' => 'confirmPassword',
                'render' => ['C', 'U'],
                'type' => 'passwordInput'
            ]
        ];
        return Yii::$app->arrayHelper::merge($parentFormColumns, $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [
            'username' => [
                'visible' => true
            ],
            'email' => [
                'visible' => true
            ],
            'auth_key' => [
                'visible' => false
            ],
            'confirmed_email' => [
                'visible' => false
            ],
            'blocked' => [
                'visible' => false
            ],
            'accepted_terms' => [
                'visible' => false
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'visible' => true,
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['id' => 'created_at_grid'],
                'hiddenFromExport' => true,
            ]
        ];

        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where([
                'user_id' => $id,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->where([
                'auth_key' => $token,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generar auth_key, password hash y asignarlo al modelo antes de guardarlo
     *
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!empty($this->password)) {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        }
        if (empty($this->email_key)) {
            $this->email_key = Yii::$app->security->generateRandomString();
        }
        $this->accepted_terms = $this->accepted_terms == 1 ? static::STATUS_ACTIVE : static::STATUS_INACTIVE;

        return parent::beforeSave($insert);
    }

    /**
     * Genera el password hash basado en la contraseña y lo asigna al modelo
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Obtener el usuario por su nombre de usuario
     *
     * @param string $username
     * @return Users|null
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->where([
                'username' => $username,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su llave de email
     *
     * @param string $emailKey
     * @return Users|null
     */
    public static function findByEmailKey($emailKey)
    {
        return static::find()
            ->where([
                'email_key' => $emailKey,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtener el usuario por su correo electronico
     * 
     * @return Users|null
     */
    public static function findByName($email)
    {
        return static::find()
            ->where([
                'email' => $email,
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Obtiene el usuario por su correo electrónico o nombre de usuario
     * 
     * @return Users|null
     */
    public static function findByUsernameOrEmail($emailUsername)
    {
        return static::find()
            ->where([
                static::STATUS_COLUMN => static::STATUS_ACTIVE,
            ])
            ->andWhere([
                'or',
                ['username' => $emailUsername],
                ['email' => $emailUsername]
            ])->one();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getItemsAssignment()
    {
        return [
            'available' => AuthItem::getAvailableItems($this->user_id),
            'assigned' => AuthItem::getAssignedItems($this->user_id)
        ];
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Enviar el correo de confirmacion de la cuenta al email de usuario
     *
     * @return void
     */
    public function sendEmailConfirmation($email)
    {
        $user = static::findByName($email);
        $url = 'security/active-account';
        $template = '@ticmakers/adm/mail/email_confirmation';
        if (Yii::$app->hasModule('notifications')) {

            $notificationModule = Yii::$app->getModule('notifications');
            $channel = $notificationModule->getChannel('E' /*EmailChannel::TYPE_CHANNEL*/);
            $channel->template = $template;

            $notificationModel = new Notifications([
                'type' => $channel->type,
                'title' => 'Confirmación de la cuenta',
                'message' => 'confirmación',
                'additional_information' => Json::encode([
                    'applicationName' => Yii::$app->name,
                    'username' => $user->username,
                    'logo' => Yii::getAlias('@app') . '/web/imagenes/logo_plantilla_correo.png',
                    'urlConfirmation' => Url::to([$url, 'token' => $user->email_key], true),
                ]),
                'generated_from' => $this->module->id,
            ]);
            $notificationModel->save();
            $recipentsModel = new NotificationRecipients([
                'notification_id' => $notificationModel->notification_id,
                'recipient' => $email,
                'user_id' => $user->user_id,
                'status' => 'P',
            ]);
            $recipentsModel->save();

            return $channel->send($notificationModel);
        } else {
            throw new Exception("Error Processing Request", 1);
        }
    }

    /**
     * Enviar el correo de restauración de contraseña para un usario registrado
     *
     * @return void
     */
    public function sendRestorePasswordEmail()
    {
        $url = 'security/change-password';
        $typeChannel = 'E';
        $template = '@ticmakers/adm/mail/reset_password';
        if (Yii::$app->hasModule('notifications')) {
            $this->validatePasswordResetToken();
            $channel = self::getChannelEmail($typeChannel, $template);
            $notificationModel = new Notifications([
                'type' => $channel->type,
                'title' => 'Cambio de contraseña',
                'message' => 'Cambio contraseña',
                'additional_information' => Json::encode([
                    'applicationName' => Yii::$app->name,
                    'username' => $this->username,
                    'logo' => Yii::getAlias('@app') . '/web/imagenes/logo_plantilla_correo.png',
                    'urlConfirmation' => Url::to([$url, 'token' => $this->password_reset_token], true),
                ]),
                'generated_from' => $this->module->id,
            ]);
            $notificationModel->save();
            $recipentsModel = new NotificationRecipients([
                'notification_id' => $notificationModel->notification_id,
                'recipient' => $this->email,
                'user_id' => $this->user_id,
                'status' => 'P',
            ]);
            $recipentsModel->save();
            return $channel->send($notificationModel);
        } else {
            throw new Exception("Error Processing Request", 1);
        }
    }

    /**
     * Permite obtener el canal de envio de notificaicon para las notificaciones tipo email
     * 
     * @return Object $channel | canal de la notificacion tipo email
     */
    public function getChannelEmail($type, $template)
    {
        $notificationModule = Yii::$app->getModule('notifications');
        $channel = $notificationModule->getChannel($type);
        $channel->template = $template;
        return $channel;
    }
    /**
     * Valida el token para cambio de contraseña para generar uno en caso de que no exista
     * 
     * @return boolean
     */
    public function validatePasswordResetToken()
    {

        if (empty($this->password_reset_token)) {
            $this->generatePasswordResetToken();
        }
        $this->save();
    }

    /**
     * Validar tiempo de expiración para el token de cambio de contraseña
     * 
     * @return boolean
     */
    public function validateExpireTimeToken()
    {
        $expireTime = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $this->password_reset_token);
        $timestamp = (int) end($parts);
        return ($timestamp + $expireTime) >= time();
    }
}
