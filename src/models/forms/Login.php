<?php

namespace ticmakers\adm\models\forms;

use Yii;
use ticmakers\adm\models\base\Users;

/**
 *
 * Modelo para control de inicio de sesión en el sistema.
 *
 * @package ticmakers
 * @subpackage adm/models/forms
 * @category Models
 *
 * @property string $username Nombre de usuario.
 * @property string $password Contraseña del usuario.
 * @property boolean $remindMe Si la sesión expira por tiempo o por cerrar.
 * @property Users $_user Instancia del usuario que quiere loggearse.
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Login extends Users
{
    public $username;
    public $password;
    public $rememberMe;

    private $_user = null;

    const USERNAME = 'username';
    const PASS = 'password';
    const REMEMBERME = 'rememberMe';

    /**
     * Definición de las reglas de validación.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [[self::USERNAME, self::PASS], 'required'],
            [[self::USERNAME], 'string', 'min' => '6', 'max' => '255'],
            [self::PASS, 'string', 'min' => '6', 'max' => '128'],
            [self::PASS, 'validatePasswordRule'],
            [self::REMEMBERME, 'boolean'],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            self::USERNAME => Yii::t($this->module->id, 'Username'),
            self::PASS => Yii::t($this->module->id, 'Password'),
            self::REMEMBERME => Yii::t($this->module->id, 'Remember me')
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            self::USERNAME => Yii::t($this->module->id, 'Username with which the user logs in.'),
            self::PASS => Yii::t($this->module->id, 'Password encrypted.')
        ];

        return $helps[$attribute] ?? (is_null($attribute) ? $helps : '');
    }

    /**
     * Realiza la validación de la contraseña ingresada.
     *
     * @param string $attribute el nombre del attributo a ser validado.
     * @param array $params Parámetros adicioneles que pueden ser otorgados al validador.
     */
    public function validatePasswordRule($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->{self::PASS})) {
                $this->addError($attribute, Yii::t($this->module->id, 'Incorrect username or password.'));
            }
        }
    }

    /**
     * Búsca el usuario por el correo electrónico
     *
     * @return User|null
     */
    public function getUser()
    {
        if (is_null($this->_user)) {
            $class = Yii::$app->getUser()->identityClass ?: 'ticmakers\adm\models\base\Users';
            $this->_user = $class::findByUsernameOrEmail($this->{self::USERNAME});
        }
        return $this->_user;
    }

    /**
     * Verifica los datos ingresados en el formulario
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $user = $this->getUser();
        if ($this->validate()) {
            if ($user->confirmed_email == self::STATUS_ACTIVE) {
                $duration =  $this->{self::REMEMBERME} ? Yii::$app->user->rememberTime : 0;
                return Yii::$app->getUser()->login($user, $duration);
            } else {
                $this->addError('username', Yii::t($this->module->id, 'You have not confirmed your account by email'));
                return false;
            }
        } else {
            return false;
        }
    }
}
