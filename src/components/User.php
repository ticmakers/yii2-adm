<?php

namespace ticmakers\adm\components;

use ticmakers\adm\models\base\Users;
use Yii;
use yii\web\User as YiiUser;
use ticmakers\adm\models\forms\Login;
use ticmakers\parameters\Module;

/**
 * AssetBundle AdmAssets.
 *
 * @package ticmakers/adm
 * @subpackage components
 * @category AssetBundle
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */

class User extends YiiUser
{
    public $expiredUrl = ['//adm/security/session-expired'];
    public $cookieUserEmail = '__user_email';
    public $timeExpiredCookieUserEmail = 10 * 60;
    public $identityClass = Users::class;
    public $loginUrl = ['//adm/security/login'];
    public $authTimeout = 30 * 60;
    public $enableAutoLogin = true;
    public $rememberTime = 60 * 60;
    public $idGuestUser = 0;
    public $cookieAutoLogin = '__auto_login';

    /**
     * Redirects the user browser to the login page.
     *
     * Before the redirection, the current URL (if it's not an AJAX url) will be kept as [[returnUrl]] so that
     * the user browser may be redirected back to the current page after successful login.
     *
     * Make sure you set [[loginUrl]] so that the user browser can be redirected to the specified login URL after
     * calling this method.
     *
     * Note that when [[loginUrl]] is set, calling this method will NOT terminate the application execution.
     *
     * @param bool $checkAjax whether to check if the request is an AJAX request. When this is true and the request
     * is an AJAX request, the current URL (for AJAX request) will NOT be set as the return URL.
     * @param bool $checkAcceptHeader whether to check if the request accepts HTML responses. Defaults to `true`. When this is true and
     * the request does not accept HTML responses the current URL will not be SET as the return URL. Also instead of
     * redirecting the user an ForbiddenHttpException is thrown. This parameter is available since version 2.0.8.
     * @return Response the redirection response if [[expiredUrl]] is set
     * @throws ForbiddenHttpException the "Access Denied" HTTP exception if [[expiredUrl]] is not set or a redirect is
     * not applicable.
     */
    public function loginRequired($checkAjax = true, $checkAcceptHeader = true)
    {
        $request = Yii::$app->getRequest();
        $canRedirect = !$checkAcceptHeader || $this->checkRedirectAcceptable();
        if (
            $this->enableSession
            && $request->getIsGet()
            && (!$checkAjax || !$request->getIsAjax())
            && $canRedirect
        ) {
            $this->setReturnUrl($request->getUrl());
        }
        if ($this->expiredUrl !== null && $canRedirect) {
            $expiredUrl = (array) $this->expiredUrl;
            if ($expiredUrl[0] !== Yii::$app->requestedRoute) {
                return Yii::$app->getResponse()->redirect($this->expiredUrl);
            }
        }
        throw new yii\web\ForbiddenHttpException(Yii::t('yii', 'Login Required'));
    }

    /**
     * This method is called after the user is successfully logged in.
     * The default implementation will trigger the [[EVENT_AFTER_LOGIN]] event.
     * If you override this method, make sure you call the parent implementation
     * so that the event is triggered.
     * @param IdentityInterface $identity the user identity information
     * @param bool $cookieBased whether the login is cookie-based
     * @param int $duration number of seconds that the user can remain in logged-in status.
     * If 0, it means login till the user closes the browser or the session is manually destroyed.
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
        parent::afterLogin($identity, $cookieBased, $duration);

        // get the cookie collection (yii\web\CookieCollection) from the "response" component
        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => $this->cookieUserEmail,
            'value' => $identity->email,
        ]));

        $cookies->add(new \yii\web\Cookie([
            'name' => $this->cookieAutoLogin,
            'value' => $duration > 0,
        ]));
    }

    /**
     * This method is invoked right after a user is logged out via [[logout()]].
     * The default implementation will trigger the [[EVENT_AFTER_LOGOUT]] event.
     * If you override this method, make sure you call the parent implementation
     * so that the event is triggered.
     * @param IdentityInterface $identity the user identity information
     */
    protected function afterLogout($identity)
    {
        parent::afterLogout($identity);
        // get the cookie collection (yii\web\CookieCollection) from the "response" component
        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => $this->cookieUserEmail,
            'value' => $identity->email,
            'expire' => time() + $this->timeExpiredCookieUserEmail,
        ]));
    }

    /**
     * Logs out the current user.
     * This will remove authentication-related session data.
     * If `$destroySession` is true, all session data will be removed.
     * @param bool $destroySession whether to destroy the whole session. Defaults to true.
     * This parameter is ignored if [[enableSession]] is false.
     * @return bool whether the user is logged out
     */
    public function logout($destroySession = true)
    {
        $identity = $this->getIdentity();
        if ($identity !== null && $this->beforeLogout($identity)) {
            $this->switchIdentity(null);
            $id = $identity->getId();
            $ip = Yii::$app->getRequest()->getUserIP();
            Yii::info("User '$id' logged out from $ip.", __METHOD__);
            $cookies = Yii::$app->response->cookies;
            $cookies->remove('_autoLogin');
            $this->afterLogout($identity);

            if ($destroySession && $this->enableSession) {
                Yii::$app->getSession()->destroy();
                // get the cookie collection (yii\web\CookieCollection) from the "response" component

                // remove a cookie
                $cookies->remove($this->cookieUserEmail);
            }
        }
        return $this->getIsGuest();
    }

    /**
     * Returns a value indicating whether the user is a guest (not authenticated).
     * @return bool whether the current user is a guest.
     * @see getIdentity()
     */
    public function getIsGuest()
    {
        $isGuest = false;
        $identity = $this->getIdentity();

        if ($identity === null) {
            $invitado = Users::findIdentity($this->idGuestUser);
            $this->setIdentity($invitado);
            $isGuest = true;
        } else if ($identity->getId() === $this->idGuestUser) {
            $isGuest = true;
        }

        return $isGuest;
    }

    /**
     * Updates the authentication status using the information from session and cookie.
     *
     * This method will try to determine the user identity using the [[idParam]] session variable.
     *
     * If [[authTimeout]] is set, this method will refresh the timer.
     *
     * If the user identity cannot be determined by session, this method will try to [[loginByCookie()|login by cookie]]
     * if [[enableAutoLogin]] is true.
     */
    protected function renewAuthStatus()
    {
        $session = Yii::$app->getSession();
        $id = $session->getHasSessionId() || $session->getIsActive() ? $session->get($this->idParam) : null;

        if ($id === null) {
            $identity = null;
        } else {
            /* @var $class IdentityInterface */
            $class = $this->identityClass;
            $identity = $class::findIdentity($id);
        }

        $this->setIdentity($identity);
        if ($identity !== null && ($this->authTimeout !== null || $this->absoluteAuthTimeout !== null)) {
            $expire = $this->authTimeout !== null ? $session->get($this->authTimeoutParam) : null;
            $expireAbsolute = $this->absoluteAuthTimeout !== null ? $session->get($this->absoluteAuthTimeoutParam) : null;

            if ($expire !== null && $expire < time() || $expireAbsolute !== null && $expireAbsolute < time()) {

                $this->logout(false);
                $isAutoLogin = Yii::$app->getRequest()->getCookies()->getValue($this->cookieAutoLogin);

                if (!$isAutoLogin) {
                    $this->loginRequired();
                }
            } elseif ($this->authTimeout !== null) {
                $session->set($this->authTimeoutParam, time() + $this->authTimeout);
            }
        }

        if ($this->enableAutoLogin) {
            if ($this->getIsGuest()) {
                $this->loginByCookie();
            } elseif ($this->autoRenewCookie) {
                $this->renewIdentityCookie();
            }
        }
    }
}
