<?php

namespace ticmakers\adm\components\actions;

use ticmakers\adm\models\base\Users;

use Yii;
use yii\web\Response;

/**
 * Acción SessionExpiredAction Permite definir el comportamiento para la expiración de la sesión en el sistema
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class ChangePasswordAction extends BaseAction
{
    /**
     * Permite ejecutar la acción para registrar un usuario
     *
     * @return mixed
     */
    public function run($token)
    {
        $modelObject = Yii::createObject(['class' => Yii::$app->user->identityClass]);
        $model = $modelObject::findByPasswordResetToken($token);

        if (!empty($model)) {
            $model->scenario = Users::SCENARIO_CHANGE;

            if ($model->validateExpireTimeToken()) {
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->save()) {

                        if ($this->isModal) {
                            return [
                                'state' => Yii::$app->message::TYPE_DANGER,
                                'message' => Yii::t($this->module->id, 'it was updated successfully'),
                                'redirect' => true,
                                'url' => Yii::$app->user->loginUrl,
                            ];
                        } else {
                            Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($this->module->id, 'it was updated successfully'));
                            return Yii::$app->response->redirect(Yii::$app->homeUrl);
                        }

                    } else {
                        $model->addError('password', Yii::t($this->module->id, 'There was an error trying to change password'));
                    }
                }

                if ($this->isModal) {
                    $renderHtml = $this->controller
                        ->renderAjax('_change_password', [
                            'model' => $model,
                            'moduleId' => $this->module->id,
                        ]);
                } else {
                    $renderHtml = $this->controller
                        ->render('_change_password', [
                            'model' => $model,
                            'moduleId' => $this->module->id,
                        ]);
                }
                Yii::$app->response->format = Response::FORMAT_HTML;
                return $renderHtml;
            } else {
                if ($this->isModal) {
                    return [
                        'state' => Yii::$app->message::TYPE_DANGER,
                        'message' => Yii::t($this->module->id, 'Password reset token has been expired'),
                        'redirect' => true,
                        'url' => Yii::$app->user->loginUrl,
                    ];
                } else {
                    Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'Password reset token has been expired'));
                    return Yii::$app->response->redirect(Yii::$app->user->loginUrl);
                }
            }
        } else {
            if ($this->isModal) {
                return [
                    'state' => Yii::$app->message::TYPE_SUCCESS,
                    'message' => Yii::t($this->module->id, 'Redirecting...'),
                    'redirect' => true,
                    'url' => Yii::$app->user->loginUrl,
                ];
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'There is no user associated with your confirmation code'));
                return Yii::$app->response->redirect(Yii::$app->user->loginUrl);
            }
        }

    }

}
