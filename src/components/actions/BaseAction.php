<?php

namespace ticmakers\adm\components\actions;

use ticmakers\adm\components\AdmAssets;
use ticmakers\adm\Module;
use Yii;
use yii\base\Action;
use yii\web\Response;

/**
 * Acción BaseAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class BaseAction extends \ticmakers\core\actions\BaseAction
{

    /**
     * This method is called right before `run()` is executed.
     * You may override this method to do preparation work for the action run.
     * If the method returns false, it will cancel the action.
     *
     * @return bool whether to run the action.
     */
    protected function beforeRun()
    {
        parent::beforeRun();
        AdmAssets::register(Yii::$app->view);
        $this->module = (Module::getInstance()) ?? Yii::$app;
        return true;
    }
}
