<?php

namespace ticmakers\adm\components\actions;

use ticmakers\adm\components\actions\BaseAction;
use ticmakers\core\components\DynamicModel;

use Yii;
use yii\web\Response;
use yii\helpers\Url;

/**
 * Acción RegisterAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class RestorePasswordAction extends BaseAction
{

    /**
     * Permite ejecutar la acción para enviar el correo de restauración de contraseña
     *
     * @return mixed
     */
    public function run()
    {
        $restoreModel = new DynamicModel(['email']);
        $restoreModel->addRule('email', 'match',
            ['pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'])
            ->addRule('email', 'string', ['max' => 50])
            ->addRule('email', 'required');
        $restoreModel->setAttributeLabels(['email' => Yii::t($this->module->id, 'Email')]);
        if ($restoreModel->load(Yii::$app->request->post())) {
            $modelUser = Yii::createObject(['class' => Yii::$app->user->identityClass]);
            $user = $modelUser::findByName($restoreModel->email);
            if (!empty($user)) {
                if ($modelUser->sendRestorePasswordEmail($user)) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    if ($this->isModal) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'state' => null,
                            'message' => null,
                            'errors' => null,
                            'type' => 'open-load-modal',
                            'url' => Url::to([ 'confirm-restore-password', 'user' => $user->primaryKey ]),
                            'modal' => 'default-modal'
                        ];
                    } else {
                        return $this->controller->render('confirm_change_password', ['model' => $user]);
                    }
                } else {
                    if ($this->isModal) {
                        return [
                            'state' => Yii::$app->message::TYPE_DANGER,
                            'message' => Yii::t($this->module->id, 'an error occurred while trying to send the mail'),
                            'errors' => null,
                            'type' => 'message',
                            'url' => null,
                            'modal' => 'default-modal'
                        ];
                    } else {
                        Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'an error occurred while trying to send the mail'));
                    }
                }
            } else {
                if ($this->isModal) {
                    return [
                        'state' => Yii::$app->message::TYPE_DANGER,
                        'message' => Yii::t($this->module->id, 'This email is not registered, please try again'),
                        'errors' => null,
                        'type' => 'message',
                        'url' => null,
                        'modal' => null
                    ];
                } else {
                    Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'This email is not registered, please try again'));
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_HTML;

        if ($this->isModal) {
            return $this->controller->renderAjax('_restore_password',
                [
                    'model' => $restoreModel,
                    'moduleId' => $this->module->id,
                ]
            );
        } else {
            return $this->controller->render('_restore_password',
                [
                    'model' => $restoreModel,
                    'moduleId' => $this->module->id,
                ]
            );
        }

    }

}
