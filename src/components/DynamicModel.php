<?php

namespace ticmakers\adm\components;

use ticmakers\core\components\DynamicModel as DynamicModelBase;

/**
 * Modelo base
 *
 * @package ticmakers
 * @subpackage adm
 * @category Components
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class DynamicModel extends DynamicModelBase
{ }
