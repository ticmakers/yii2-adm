<?php

namespace ticmakers\adm\components\rbac;

use ticmakers\adm\models\base\AuthItem;

class Role extends AuthItem
{
    /**
     * {@inheritdoc}
     */
    public $type = AuthItem::TYPE_ROL;
}
