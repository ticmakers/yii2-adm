<?php

namespace ticmakers\adm\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "users".
 * This entity stores all the users.
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $user_id User identifier
 * @property string $username Username with which the user logs in.
 * @property string $email User email
 * @property string $password_hash Password encrypted.
 * @property string $auth_key Random code that identifies the access key for resources by the api rest
 * @property string $password_reset_token Random code to identify when to reset the password
 * @property string $email_key Key to validate that the email is valid
 * @property string $confirmed_email Establishes if the email is confirmed. Format  Y -> Yes  N -> No
 * @property string $blocked Establishes if the user is blocked  Y -> Yes  N -> No
 * @property string $accepted_terms It determines if the user accepts or not the terms and conditions.  Format: Y->Yes or N->No
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by  It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Users extends \ticmakers\adm\models\base\Users
{ }
