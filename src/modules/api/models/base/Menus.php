<?php

namespace ticmakers\adm\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "menus".
 * This entity stores all the menus.
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $menu_id Menu identifier
 * @property string $name Menu name
 * @property string $description Menu's description
 * @property string $type Define the menu type:  B-> Backend F-> Frontend M -> Movil
 * @property string $position Define the location of the menu
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Menus extends \ticmakers\adm\models\base\Menus
{ }
