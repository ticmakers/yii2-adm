<?php

namespace ticmakers\adm\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_rule".
 * This entity stores all the rules
 *
 * @package ticmakers/adm
 * @subpackage models/base
 * @category models
 *
 * @property string $name Rule identifier
 * @property string $data Additional value of the rule identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class AuthRule extends \ticmakers\adm\models\base\AuthRule
{ }
