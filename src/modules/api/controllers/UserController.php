<?php

namespace ticmakers\adm\modules\api\controllers;

use ticmakers\core\rest\ActiveController;

/**
 * UserController Clase encargada de presentar y manipular la información del modelo User para las solicitudes en el api
 *
 * @package app/modules
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 
 * @copyright (c) 2019, TIC Makers S.A.S
 * @version 0.0.1
 */
class UserController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = \ticmakers\adm\modules\api\models\base\Users::class;

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = \ticmakers\adm\modules\api\models\searchs\Users::class;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'user_id';
}
