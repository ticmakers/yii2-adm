<?php

namespace ticmakers\adm\modules\api\controllers\actions;

use ticmakers\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use ticmakers\core\components\DynamicModel;

/**
 * Acción VerifyTokenAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class LogoutAction extends BaseAction
{
    /**
     * Permite ejecutar la acción para validar un token
     *
     * @return mixed
     */
    public function run()
    {

        $userObject = Yii::$app->user->identity;
        $userObject->auth_key = Yii::$app->security->generateRandomString();
        $userObject->update(false, ['auth_key']);

        return [
            'message' => Yii::t('app', $this->messageOnSuccess)
        ];
    }
}
