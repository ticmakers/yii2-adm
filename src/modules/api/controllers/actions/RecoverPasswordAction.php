<?php

namespace ticmakers\adm\modules\api\controllers\actions;

use ticmakers\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use ticmakers\core\components\DynamicModel;

/**
 * Acción RecoverPasswordAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class RecoverPasswordAction extends BaseAction
{
    public $modelValidate;
    public $modelUser;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'We have sent a message to your email with a link to confirm your password change request.';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnError = 'Operation failed';


    /**
     * Permite ejecutar la acción para validar un token
     *
     * @return mixed
     */
    public function run()
    {
        $modelClass = Yii::$container->get(Yii::$app->user->identityClass);
        $this->modelValidate = DynamicModel::withRules([
            'email' => Yii::$app->request->post('email')
        ], [
            [['email'], 'required'],
            [['email'], 'match', 'pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'],
            [['email'], 'string', 'max' => 255],
            [
                'email',
                'exist',
                'targetClass' => $modelClass,
                'targetAttribute' => ['email' => 'email'],
                'filter' => function ($query) {
                    $modelClass = Yii::$container->get(Yii::$app->user->identityClass);
                    $query->andWhere([$modelClass::STATUS_COLUMN => $modelClass::STATUS_ACTIVE]);
                }
            ]
        ]);

        if ($this->modelValidate->validate()) {
            $this->modelUser = $modelClass->findByName($this->modelValidate->email);
            $this->modelUser->sendRestorePasswordEmail();
            $result =  [
                'message' => Yii::t('app', $this->messageOnSuccess)
            ];
        } else {
            $result = $this->modelValidate;
        }

        return $result;
    }
}
