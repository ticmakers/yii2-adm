<?php

namespace ticmakers\adm\modules\api\controllers\actions;

use Yii;
use ticmakers\adm\modules\api\controllers\actions\BaseAction;
use ticmakers\core\components\DynamicModel;

/**
 * LoginAction clase tipo acción que puede ser implementada en un controlador para el manejo del login de los usuarios.
 *
 * @property string $view Vista que se renderizará para el formulario de login.
 *
 * @package ticmakers
 * @subpackage adm\components\actions
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class LoginAction extends BaseAction
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'Successful operation';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnError = 'The data entered is invalid, please check and try again.';
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $identityClass = \ticmakers\adm\modules\api\models\searchs\Users::class;

    /**
     * Permite ejecutar la acción para registrar un usuario
     *
     * @return mixed
     */
    public function run()
    {
        if (Yii::$app->user->isGuest) {

            $model = DynamicModel::withRules([
                'email' => Yii::$app->request->post('email'),
                'password' => Yii::$app->request->post('password'),
                'notification_id' => Yii::$app->request->post('notification_id')
            ], [
                [['email', 'password'], 'required'],
                [['password'], 'string', 'min' => 8, 'max' => 16],
                [['email'], 'match', 'pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'],
                [['email'], 'string', 'max' => 255],
                ['notification_id', 'string', 'max' => 128]
            ]);

            if (!$model->validate()) {
                return $model;
            }

            $userObject = Yii::$container->get($this->identityClass)::findByName($model->email);

            if (!($userObject && $userObject->validatePassword($model->password))) {
                throw new \yii\web\HttpException(401, $this->messageOnError);
            }

            $userObject->auth_key = Yii::$app->security->generateRandomString();
            $userObject->update(false, ['auth_key']);

            if (property_exists($userObject, 'notification_id')) {
                $userObject->notification_id = $model->notification_id;
                if (!$userObject->save(false, ['notification_id'])) {
                    throw new \yii\web\HttpException(422, 'It was not possible to update the device ID.');
                }
            }
            Yii::$app->user->loginByAccessToken($userObject->auth_key);
        } else {
            $userObject = Yii::$app->user->identity;
        }
        $modelUserApi = Yii::createObject($this->identityClass);
        return $modelUserApi::findOne($userObject->primaryKey);
    }
}
