<?php

namespace ticmakers\adm\modules\api\controllers\actions;

use ticmakers\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use ticmakers\core\components\DynamicModel;

/**
 * Acción VerifyTokenAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class VerifyTokenAction extends BaseAction
{
    public $modelValidate;
    public $modelUser;

    /**
     * Permite ejecutar la acción para validar un token
     *
     * @return mixed
     */
    public function run()
    {
        return Yii::createObject(\ticmakers\adm\modules\api\models\searchs\Users::class)::findOne(Yii::$app->user->identity->id);
    }
}
