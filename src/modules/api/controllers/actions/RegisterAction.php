<?php

namespace ticmakers\adm\modules\api\controllers\actions;

use ticmakers\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use ticmakers\core\components\DynamicModel;

/**
 * Acción RegisterAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class RegisterAction extends BaseAction
{
    const EVENT_API_AFTER_USER_CREATE = 'event-api-after-user-create';
    public $modelValidate;
    public $modelUser;
    public $messageOk = 'Registro exitoso';

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_API_AFTER_USER_CREATE, [$this, 'afterUserCreate']);
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function afterUserCreate($event)
    { }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function beforeRegisterValidate(&$modelValidate)
    { }

    /**
     * Permite ejecutar la acción para registrar un usuario
     *
     * @return mixed
     */
    public function run()
    {
        $result = '';
        $this->modelValidate = DynamicModel::withRules([
            'email' => Yii::$app->request->post('email'),
            'name' => Yii::$app->request->post('name'),
            'password' => Yii::$app->request->post('password'),
            'confirm_password' => Yii::$app->request->post('confirm_password')
        ], [
            [['email', 'name', 'password', 'confirm_password'], 'required'],
            ['password', 'compare', 'compareAttribute' => 'confirm_password'],
            [['password'], 'string', 'min' => 8, 'max' => 16],
            [['email'], 'match', 'pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'],
            [['email'], 'string', 'max' => 255],
            [
                ['email'], 'unique', 'targetClass' => Yii::$container->get(Yii::$app->user->identityClass),
                'message' => Yii::t('app', "No es posible crear el usuario debido a que el {attribute}, ya se encuentra registrado.")
            ]
        ]);

        $this->beforeRegisterValidate($this->modelValidate);

        if (!$this->modelValidate->validate()) {
            $result = $this->modelValidate;
        } else {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $this->modelUser = Yii::createObject(Yii::$app->user->identityClass);
                $this->modelUser->username = $this->modelValidate->name;
                $this->modelUser->email = $this->modelValidate->email;
                $this->modelUser->password = $this->modelValidate->password;
                $this->modelUser->confirmed_email = $this->modelUser::STATUS_INACTIVE;
                $this->modelUser->save();
                $this->trigger(self::EVENT_API_AFTER_USER_CREATE);
                $this->modelUser->sendEmailConfirmation($this->modelUser->email);
                $transaction->commit();
                $result = ["message" => Yii::t('app', $this->messageOk)];
            } catch (\Throwable $th) {
                $transaction->rollBack();
                throw $th;
            }
        }

        return $result;
    }
}
