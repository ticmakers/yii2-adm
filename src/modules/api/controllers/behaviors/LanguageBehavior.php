<?php

namespace ticmakers\adm\modules\api\controllers\behaviors;

use Yii;

/**
 * Acción BaseAction.
 *
 * @package ticmakers/adm
 * @subpackage components/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class LanguageBehavior extends \yii\base\Behavior
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function events()
    {
        return [
            \yii\base\Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function beforeAction($event)
    {
        if (Yii::$app->request->isGet) {
            $language = Yii::$app->request->get('language', Yii::$app->language);
        } else if (Yii::$app->request->isPost) {
            $language = Yii::$app->request->post('language', Yii::$app->language);
        }
        Yii::$app->language = $language;
        return true;
    }
}
