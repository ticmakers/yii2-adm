<?php

namespace ticmakers\adm\modules\api;

use Yii;

use yii\base\Application;
use yii\base\BootstrapInterface;
use ticmakers\adm\modules\api\models\base\AuthAssignment;
use ticmakers\adm\modules\api\models\base\AuthItem;
use ticmakers\adm\modules\api\models\base\AuthItemChild;
use ticmakers\adm\modules\api\models\base\AuthRule;
use ticmakers\adm\modules\api\models\base\MenuItems;
use ticmakers\adm\modules\api\models\base\Menus;
use ticmakers\adm\modules\api\models\base\RoleMenuItems;
use ticmakers\adm\modules\api\models\base\SocialNetworks;
use ticmakers\adm\modules\api\models\base\Users;

use ticmakers\adm\modules\api\models\searchs\AuthAssignment as AuthAssignmentSearch;
use ticmakers\adm\modules\api\models\searchs\AuthItem as AuthItemSearch;
use ticmakers\adm\modules\api\models\searchs\AuthItemChild as AuthItemChildSearch;
use ticmakers\adm\modules\api\models\searchs\MenuItems as MenuItemsSearch;
use ticmakers\adm\modules\api\models\searchs\Menus as MenusSearch;
use ticmakers\adm\modules\api\models\searchs\Users as UsersSearch;

/**
 * Class Bootstrap
 * @package ticmakers\yii2-adm
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [
            'AuthAssignment' => AuthAssignment::class,
            'AuthItem' => AuthItem::class,
            'AuthItemChild' => AuthItemChild::class,
            'AuthRule' => AuthRule::class,
            'MenuItems' => MenuItems::class,
            'Menus' => Menus::class,
            'RoleMenuItems' => RoleMenuItems::class,
            'SocialNetworks' => SocialNetworks::class,
            'Users' => Users::class,
        ],
        'searchs' => [
            'AuthAssignment' => AuthAssignmentSearch::class,
            'AuthItem' => AuthItemSearch::class,
            'AuthItemChild' => AuthItemChildSearch::class,
            'MenuItems' => MenuItemsSearch::class,
            'Menus' => MenusSearch::class,
            'Users' => UsersSearch::class,
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('api')) {
            $app->setModule('api', 'ticmakers\adm\modules\api\Module');
        }
        $this->overrideModels($app, 'api');
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "ticmakers\\adm\\modules\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}
